# SpotiPy

SpotiPy is a music client which can play music from [Spotify](http://www.spotify.com) or from your local hard drive.

Upon migrating source to git, I have removed the last.fm and spotify keys for their applications. To set these values with your own, edit `spotipy/spotipy.py` for the spotify key and `spotipy/spotipy/frontends/lastfm.py` for the last.fm api key and secret.

--------------------------------------------------------------------------------

## Installation Requirements

- Python 2.7
- Gnome3/Unity/GTK3
- python-mutagen (used for reading music file metadata; id3 info)
- python-sqlite (used for indexing local files)

# Optional

- [Premium Spotify Account](http://spotify.com)
- [libspotify-dev](http://apt.mopidy.com/) (gets latest version of libspotify)
- [python-spotify](http://pyspotify.mopidy.com/docs/master/) OR `git clone https://github.com/mopidy/pyspotify` and then build/install the latest version of the python-spotify package.
- python-alsaaudio OR python-oss (used for playing spotify audio)
- AppIndicator3 from gi.repository
- Notify from gi.repository
- [python-pylast](http://code.google.com/p/pylast/) (used for last.fm access)
- [python-pykka](http://jodal.github.com/pykka/) (used for last.fm AND Notify integration)
- python-pylast and python-pykka are available from the [mopidy ppa](http://apt.mopidy.com) or can be installed from source.

# Installation

- Run `python setup.py build`
- then `sudo python setup.py install`
- Run the application `spotipy` or locate in the menu.
- Enter Spotify Credentials.
- The tool bar on the upper right can be used to login to last.fm as well as change audio quality of spotify.
- Settings are automatically saved in ~/.config/spotipy/settings.json file for future instantiations.

--------------------------------------------------------------------------------

## DEVELOPMENT

- Development of Spotify integration is halted (I no longer subscribe).
- If you want to help. please let me know. spotipy at tros.org

# KNOWN ISSUES

- It seems that if you press "next" a lot and possibly queue up last.fm requests, and then close quickly, the application hangs on closing after the window disappears.
- Known that local tracks from spotify will not play or that tracks from spoitfy come back as non-playable, need to check this before playing as this causes the playlist to stop until "Next" is pressed.
- Possible problem w/ some URI values. '?' for example in "Are You Passionate?" etc.
- Possible that 2 consecutive spaces in a song path causes an issue.

# GUI development

- GUI's are not my strong point. I have 2 sets of GUI files (.glade and .ui).
- The reason for this is that glade 3.10 crashes when configuring a TreeView, so I use 3.8 and then hand convert/compare to be compatible with gtk3.
- This is less of an issue now that I have opted to use html/WebKit as the main portal for viewing data. A TreeView is still used to display folders and playlists.

# Goals

- A spotify clone where any gstreamer local file can be played instead of only mp3/m4a files.
- Now envisioning a tighter integration with Last.fm

# TODO (in no particular order)

- Port to Python3
- [In Dev] Show/goto current file (in application)
- Find a way to detect scrolling to know if we should get more search results from Spotify
- Remove duplicate albums from album-browsing in Spotify
- Detect duplicates in playlist. (duplicates can be done with "set" function)
- Show "Top" from Spotify
- Playlist modifications (both detect and change from within Spotipy)
- Search Last FM for tracks of interest that are on Spotify/Local
- Searching local DB
- Show Friends (not sure if this is useful given that the library doesn't import their playlists, I think)
- Equalizer Support
- Volume Support
- Possibly use GStreamer for media playback from spotify instead of pulseaudio (as mopidy does)
- Add support for gi.repository.Gst instead of using the older gst
- DB - improve searches. Known search that breaks is "Love Hate and then There's you". Single quote not in Spotify tag. Be able to work around small infractions in SQL queries.
- Add MusicBrainz id from lastfm lookups.

# THANKS

- My wife for letting me continue on this endeavor.
- The [pyspotify project](http://pyspotify.mopidy.com).
- The [mopidy project](http://mopidy.com).

# RELEASE NOTES

## 0.0.15

- Display artist and album results from search
- Links to Albums and Artists for increased detail
- Back/Forward buttons (keep track of history of HTML)
- Fixed issue where search results weren't properly kept/displayed.

## 0.0.14

- Add functionality w/ Music Menu under Ubuntu (preliminary)
- Added Notifications for new tracks playing
- Added Initial AppIndicator3 support
- Enhancing results returned from search
- Small UI fixes/enhancements
- Hilite/show current track

## 0.0.13

- Beta use of HTML for displaying tracks. This still requires building the queue list when playing as well as enabling "starring" and possible notification for if it is a spotify track or local track.
- Would like to eventually be able to re-add sorting on the table as well as searching.

## 0.0.12

- Last FM Bio and AlbumArt
- Use of WebKit for displaying artist bio.
- Playing searched files should work again.
- Save Settings
- Create all config/data/cache directories
- Fixed audio quality not being set for spotify.

## 0.0.11

- pykka
- Windows for editing preferences (login data).

## 0.0.10

- last.fm

## 0.0.9

- Indexing local files
- Starring local files
- DB Lookup of spotify files to see if a (for now) FLAC version of the file exists. If so, play it.
- Icons to show "network" filese (Spotify) and "file" for local file.

## 0.0.8

- Re-engineer playlist wrapping for ease of extension.
- TODO: username is now no longer printed on the application.

## 0.0.7

- Queuing
- Shuffle
- Repeat
- Detect duplicate tracks in playlist (nothing done about it, not added to screen)
- Star using toggle button in UI
- Searching (Spotify); shows tracks only
- Show play history/queue
- Show user info (Name)
- Show current file (in status bar)
- Playing a current directory/playlist
- Layout/UI modifications
- Abstractions of wrappers and audio files

## 0.0.6

- Track seek and position knowledge for both spotify and gstreamer
- History of tracks kept
- Able to go back to previous tracks

## 0.0.5 and earlier

- Initial development and proof-of-concepts
