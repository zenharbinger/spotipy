# -*- coding: utf-8 -*-
__author__ = "Matthew Aguirre"
__copyright__ = "Copyright 2011-2016, Matthew Aguirre"
__credits__ = ["Matthew Aguirre", "SPOTIFY(R) CORE", "pyspotify"]
__license__ =  """Licensed under the Apache License, Version 2.0 (the “License”);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS
OF ANY KIND, either express or implied. See the License for the specific language
governing permissions and limitations under the License.""" 
__version__ = "0.0.15"
__maintainer__ = "Matthew Aguirre"
__email__ = "matt.aguirre@gmail.com"
__status__ = "Development"

import sys
from gi.repository import GLib as glib
import os

import logging
logging.basicConfig()

DATA_PATH = os.path.join(glib.get_user_data_dir(), 'spotipy')
CACHE_PATH = os.path.join(glib.get_user_cache_dir(), 'spotipy')
SETTINGS_PATH = os.path.join(glib.get_user_config_dir(), 'spotipy')
SETTINGS_FILE = os.path.join(SETTINGS_PATH, 'settings.py')
SETTINGS_JSON_FILE = os.path.join(SETTINGS_PATH, 'settings.json')

if not os.path.exists(SETTINGS_PATH):
    try:
        os.mkdir(SETTINGS_PATH)
    except:
        pass
if not os.path.exists(DATA_PATH):
    try:
       os.mkdir(SETTINGS_PATH)
    except:
        pass
if not os.path.exists(CACHE_PATH):
    try:
        os.mkdir(SETTINGS_PATH)
    except:
        pass

SPOTIFY_KEY=""

from spotipy import settings as default_settings_module
from spotipy.utils.settings import SettingsProxy
settings = SettingsProxy(default_settings_module)

class PlayState(object):
    NULL=1
    PLAYING=2
    PAUSED=3
    STOPPED=4

class RepeatState(object):
    NONE=1
    TRACK=2
    LIST=3

class ShuffleState(object):
    OFF=1
    ON=2

def get_version():
    return __version__
