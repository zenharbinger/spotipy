# -*- coding: utf-8 -*-

import spotipy.core.baseplaylist
import os
import mimetypes

class DummyPlayList(spotipy.core.baseplaylist.BasePlayList):
    def __init__(self, wrapper, name):
        spotipy.core.baseplaylist.BasePlayList.__init__(self, wrapper)
        self.__name = name
        self.__list = []

    def get_name(self):
        return self.__name

    def get_tracks(self):
        return self.__list
        #for x in self.__list:
        #    yield x

    def add_track(self, track):
        if track != None and not track in self.__list:
            self.__list.append(track)

    def remove_track(self, track):
        if track != None and track in self.__list:
            self.__list.remove(track)
