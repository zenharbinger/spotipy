# -*- coding: utf-8 -*-

import spotipy
from urllib import pathname2url
import os
from os.path import exists
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst as gst
gst.init(None)

from spotipy.core.basewrapper import BaseWrapper
from localplaylist import LocalPlayList
from localaudiofile import LocalAudioFile
import getpass
from localindexer import LocalIndexer
import spotipy.backends.dummy.dummyplaylist
from gi.repository import GLib as glib
import time
import logging

import traceback, sys

class GStreamerWrapper(BaseWrapper):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(GStreamerWrapper, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    @classmethod
    def get_instance(cls):
        return cls._instance

    def find_tracks(self, artist, album, title):
        l = []
        ll = self.__indexer.find(artist, album, title)
        for x in ll:
            l.append(self.create_audio_file(x))
        return l

    def __init__(self):
        self.__logger = logging.getLogger("spotipy.backends.local.GStreamerWrapper")
        self.__logger.debug("GStreamerWrapper __init__")
        self.__music_dir = spotipy.settings.LOCAL_MUSIC_PATH
        if self.__music_dir == None or self.__music_dir == "" or not os.path.exists(self.__music_dir):
            self.__music_dir = glib.get_user_special_dir(glib.USER_DIRECTORY_MUSIC)
        self.__nowplaying = None
        self.__status = spotipy.PlayState.NULL
        self.__player = None
        self.__track_end_listeners = []
        self.__indexer = LocalIndexer(self.__music_dir, self, os.path.join(spotipy.DATA_PATH, "music.db3"))
        self.__audio_files = {}
        self.__last_position = -1
        self.__last_duration = -1

        try:
            vol = self.__player.get_property('volume')
            force_volume = True
        except AttributeError:
            force_volume = False

        if self.__player is None:
            self.__player = gst.ElementFactory.make('playbin', 'spotipy')
            self.__equalizer = gst.ElementFactory.make('equalizer-3bands', 'equalizer')

            audioconvert = gst.ElementFactory.make('audioconvert', 'convert')
            audiosink = gst.ElementFactory.make('autoaudiosink', 'audio_sink')

            sinkbin = gst.Bin()
            sinkbin.add(self.__equalizer, audioconvert, audiosink)
            self.__equalizer.link(audioconvert)
            audioconvert.link(audiosink)

            sinkpad = self.__equalizer.get_static_pad('sink')
            sinkbin.add_pad(gst.GhostPad('sink', sinkpad))

            self.__player.set_property('audio-sink', sinkbin)

            bus = self.__player.get_bus()
            bus.add_signal_watch()
            bus.connect('message', self.on_message)

        if force_volume:
            self.__player.set_property('volume', vol)

    def __rec_walk(self, d, tn, wrapper):
        """ Recursively walks a directory tree to populate the treeview """
        if os.path.isdir(d):# and not os.path.islink(d):
            for f in sorted(os.listdir(unicode(d))):
                p = unicode(os.path.join(d, f))
                if os.path.isdir(p):
                    file_node = LocalPlayList(self, p)
                    file_node.set_parent_list(tn[0])
                    tn[0].add_child_list(file_node)
                    tn.insert(0, file_node)
                    self.__rec_walk(os.path.join(d, f), tn, wrapper)
                    tn.pop(0)

    def get_playlists(self, cb, *data):
        self.__logger.debug("GET PLAYLISTS")
        l = LocalPlayList(self, self.__music_dir, "File System")
        tn = [l]
        self.__rec_walk(self.__music_dir, tn, self)
        cb([l], data)
        pass

    def get_starred_state(self, track):
        self.__logger.debug("GET STARRED STATE")
        self.__indexer.populate(track)
        pass

    def set_starred_state(self, track, val):
        self.__logger.debug("SET STARRED STATE")
        self.__indexer.update(track, val)
        pass

    def get_starred(self):
        self.__logger.debug("GET STARRED")
        l = self.__indexer.get_starred()
        ll = spotipy.backends.dummy.dummyplaylist.DummyPlayList(self, "Starred")
        for f in l:
            try:
                ll.add_track(self.create_audio_file(f))
            except:
                pass
        return ll

    def get_username(self):
        return getpass.getuser()

    def add_playlist_listener(self, listener):
        listener(self)
        pass

    def get_music_dir(self):
        return self.__music_dir

    def add_track_listener(self, listener):
        self.__track_end_listeners.append(listener)

    def play(self, song):
        self.__logger.debug("PLAY")
        self.stop()
        self.__init_song(song)
        return self.create_audio_file(song)

    def pause(self):
        self.__logger.debug("PAUSE")
        if self.__nowplaying != None and self.__status == spotipy.PlayState.PLAYING:
            self.__player.set_state(gst.State.PAUSED)
            self.__status = spotipy.PlayState.PAUSED
        elif self.__nowplaying != None and self.__status == spotipy.PlayState.PAUSED:
            self.__player.set_state(gst.State.PLAYING)
            self.__status = spotipy.PlayState.PLAYING
        return self.__status

    def stop(self):
        self.__logger.debug("STOP")
        self.__player.set_state(gst.State.NULL)
        self.__nowplaying = None
        self.__status = spotipy.PlayState.STOPPED

    def shutdown(self):
        self.__logger.debug("SHUTDOWN")
        self.stop()

    def __init_song(self, song):
        self.__logger.debug("__INIT_SONG")
        self.__nowplaying = song
        self.__last_position = -1
        self.__last_duration = -1

        if song is not None and exists(song):
            song = pathname2url(song)
            self.__player.set_property('uri', 'file://' + song)
        elif song is not None:
            self.__player.set_property('uri', song)

        self.__player.set_state(gst.State.PLAYING)
        self.__status = spotipy.PlayState.PLAYING

    def __getnowplaying(self):
        self.__logger.debug("__GETNOWPLAYING")
        return self.__nowplaying

    now_playing = property(__getnowplaying)

    def __getstatus(self):
        return self.__status

    status = property(__getstatus)

    def __getplayer(self):
        return self.__player

    player = property(__getplayer)

    def __getequalizer(self):
        return self.__equalizer

    equalizer = property(__getequalizer)

    def get_position(self):
        if self.__last_position > -1:
            return self.__last_position
        x = self.__player.get_state(0);
        if x[1] == gst.State.NULL:
            return -1

        while True:
            try:
                if self.__status != spotipy.PlayState.NULL:
                    return int(self.__player.query_position(gst.Format.TIME)[1] / float(1000000))
                else:
                    return -1
            except Exception as ex:
                self.__logger.debug("GET POSITION: %s", str(ex))
        return -1

    def __seek(self, seconds):
        value = int(gst.SECOND * seconds)
        self.__player.seek_simple(gst.Format.TIME, gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, value)

    position = property(get_position)

    def get_duration(self):
        if self.__last_duration > -1:
            return self.__last_duration
        if self.__player.get_state(0)[1] == gst.State.NULL:
            return -1

        count = 0
        while True:
            try:
                if self.__status != spotipy.PlayState.NULL:
                    return int(self.__player.query_duration(gst.Format.TIME)[1] / float(1000000))
                else:
                    return -1
            except Exception as ex:
                self.__logger.debug("GET DURATION: %s", str(ex))
        return -1

    duration = property(get_duration)

    def get_norm_position(self):
        while True:
            try:
                if self.__status != spotipy.PlayState.NULL:
                    ret = float(self.__player.query_position(gst.Format.TIME)[1]) / float(self.__player.query_duration(gst.Format.TIME)[1])
                    return ret * float(100.0)
                else:
                    return -1
            except Exception as ex:
                self.__logger.debug("GET NORM POSIITON: %s", str(ex))
                pass
        return -1

    def set_norm_position(self, percent):
        per = percent / 100;
        if per > 1:
            per = 1
        if per < 0:
            per = 0
        if self.__status != spotipy.PlayState.NULL:
            try:
                value = self.__player.query_duration(gst.Format.TIME)[1] * (per)
            except Exception as ex:
                #this will probably happen when we are seeking to the end of the track.
                value = -1
            #if we have a valid seek value, go to that point in the song.
            if value >= 0:
                self.__player.seek_simple(gst.Format.TIME, gst.SeekFlags.FLUSH | gst.SeekFlags.KEY_UNIT, value)

    norm_position = property(get_norm_position, set_norm_position)

    def is_playing(self):
        return self.__status != spotipy.PlayState.NULL

    def on_message(self, bus, message):
        _type = message.type
        if _type == gst.MessageType.EOS:
            self.__logger.debug("SONG FINISHED PLAYING")
            self.__last_position = self.get_duration()
            self.__last_duration = self.__last_position

            self.__player.set_state(gst.State.NULL)
            self.__status = spotipy.PlayState.NULL

            for listener in self.__track_end_listeners:
                listener(self, self.__nowplaying)

            #BUG IS HERE!
            #self.__last_position = self.get_duration()
            #self.__last_duration = self.__last_position

        elif _type == gst.MessageType.ERROR:
            self.__player.set_state(gst.State.NULL)
            self.__nowplaying = None
            self.__status = spotipy.PlayState.NULL
            err, debug = message.parse_error()
            #print 'Error: %s' % err, debug
            self.__logger.error("Error: %s", err)

    def create_audio_file(self, data):
        if not data in self.__audio_files:
            ret = LocalAudioFile(data)
            ret.set_wrapper(self)
            self.__audio_files[data] = ret
            return ret
        else:
            return self.__audio_files[data]
 
    def start(self):
        pass
