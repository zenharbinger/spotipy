# -*- coding: utf-8 -*-

import os
import mutagen  #for getting id3 tag info on audio data
import logging
from spotipy.core.audiofile import AudioFile
from spotipy.core.artist import Artist
from spotipy.core.album import Album

class LocalAudioFile(AudioFile):
    def __init__(self, filename):
        if filename.find("file://") == 0:
            filename = filename.replace("file://", "")
        AudioFile.__init__(self, filename)
        self.__logger = logging.getLogger("spotipy.backends.local.LocalAudioFile")
        if os.path.exists(filename):
            self.__folder = os.path.dirname(filename)
            self.__filename = os.path.basename(filename)
            self.set_title(self.__filename)
        else:
            self.__logger.warning("File Not Found: %s", filename)
            raise IOError(filename)
        self.__get_data_from_file()
        self.__initialized = False
        self.__starred = False

    def get_artist(self):
        artist = AudioFile.get_artist(self)
        return Artist(artist, "artist://" + artist)
    def get_album(self):
        album = AudioFile.get_album(self)
        return Artist(album, "album://" + album)

    def get_starred(self):
        if not self.__initialized:
            self.get_wrapper().get_starred_state(self)
        return self.__starred

    def set_starred(self, value):
        val = 0
        if value:
            val = 1
        self.get_wrapper().set_starred_state(self, value)
        AudioFile.set_starred(self, value)

    def get_starred_helper(self):
        return self.__starred

    def set_starred_helper(self, value):
        self.__initialized = True
        self.__starred = value

    def get_uri(self):
        return "file://" + self.__get_path()

    def __get_path(self):
        return os.path.join(self.__folder, self.__filename)

    def __get_data_from_file(self):
        try:
            d = mutagen.File(self.__get_path(), easy=True)
        except:
            return

        try:
            if "title" in d :
                self.set_title(d["title"][0])
        except Exception as ex:
            pass
        try:
            if "artist" in d :
                self.set_artist(d["artist"][0])
        except Exception as ex:
            pass
        try:
            if "tracknumber" in d :
                self.set_tracknumber(d["tracknumber"][0])
                if not self.tracknumber.isdigit() :
                    self.set_tracknumber(0)
                else:
                    self.set_tracknumber(self.tracknumber)
        except Exception as ex:
            pass
        try:
            if "album" in d :
                self.set_album(d["album"][0])
        except Exception as ex:
            pass
        try:
            if "genre" in d :
                self.set_genre(d["genre"][0])
        except Exception as ex:
            pass
            
