# -*- coding: utf-8 -*-

import spotipy
import mimetypes
import localaudiofile
import threading
import os
import sqlite3
import logging

class LocalIndexer(threading.Thread):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(LocalIndexer, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    @classmethod
    def get_instance(cls):
        return cls._instance
        
    def __init__(self, directory, wrapper, db_file):
        threading.Thread.__init__(self)
        #self.setDaemon(True)
        self.__logger = logging.getLogger("spotipy.backends.local.LocalIndexer")
        self.__index_dir = directory
        self.__wrapper = wrapper
        self.__db_file = os.path.expanduser(db_file)
        if not os.path.exists(os.path.dirname(db_file)):
            os.mkdir(os.path.dirname(db_file))
        self.__continue = True
        self.start()
        self.name = "SpotiPyIndexer"

    def get_starred(self):
        l = []
        __conn = sqlite3.connect(self.__db_file)
        __c = __conn.cursor()
        try:
            q = u"select uri from files where starred !=0 "
            __c.execute(q)
            row = __c.fetchone()
            while row != None:
                l.append(LocalIndexer.from_db_string(row[0]))
                row = __c.fetchone()
        except Exception as ex:
            self.__logger.warning("GET STARRED: %s", str(ex))
        __conn.commit()
        __c.close()
        return l

    def find(self, album, artist, title):
        l = []
        __conn = sqlite3.connect(self.__db_file)
        __c = __conn.cursor()
        try:
            q1 = u"select uri from files where artist='" + LocalIndexer.get_db_string(artist) + u"' and album='" + LocalIndexer.get_db_string(album) + u"' and title='" + LocalIndexer.get_db_string(title) + u"' and uri LIKE '%flac'"
            q2 = u"select uri from files where artist='" + LocalIndexer.get_db_string(artist) + u"' and album LIKE '" + LocalIndexer.get_db_string(album) + u"%' and title='" + LocalIndexer.get_db_string(title) + u"' and uri LIKE '%flac'"
            q3 = u"select uri from files where artist='" + LocalIndexer.get_db_string(artist) + u"' and album LIKE '" + LocalIndexer.get_db_string(album) + u"%' and title LIKE '" + LocalIndexer.get_db_string(title) + u"%' and uri LIKE '%flac'"
            q4 = u"select uri from files where artist LIKE '%" + LocalIndexer.get_db_string(artist) + u"%' and album LIKE '%" + LocalIndexer.get_db_string(album) + u"%' and title LIKE '%" + LocalIndexer.get_db_string(title) + u"%' and uri LIKE '%flac'"
            ll = [q1, q2, q3, q4]
            for q in ll:
                __c.execute(q)
                row = __c.fetchone()
                while row != None:
                    r = LocalIndexer.from_db_string(row[0])
                    l.append(r)
                    row = __c.fetchone()
        except Exception as ex:
            self.__logger.error("FIND: %s", str(ex))
        __conn.commit()
        __c.close()
        return l
        
    def update(self, track, value):
        __conn = sqlite3.connect(self.__db_file)
        __c = __conn.cursor()
        try:
            val = 0
            if value:
                val =  1
            q = u"update files set starred=" +  LocalIndexer.get_db_string(str(val)) + " where uri='" +  LocalIndexer.get_db_string(track.get_uri()) + "'"
            __c.execute(q)
            
            track.set_starred_helper(val != 0)
        except Exception as ex:
            self.__logger.error("UPDATE: %s", str(ex))
        __conn.commit()
        __c.close()

    def populate(self, track):
        __conn = sqlite3.connect(self.__db_file)
        __c = __conn.cursor()
        try:
            q = u"select starred from files where uri='" +  LocalIndexer.get_db_string(track.get_uri()) + "'"
            __c.execute(q)
            row = __c.fetchone()

            val = 0
            if row != None:
                val = row[0]

            track.set_starred_helper(val != 0)
        except Exception as ex:
            self.__logger.error("POPULATE: %s", str(ex))

        __conn.commit()
        __c.close()

    def run(self):
        if os.path.exists(self.__index_dir):
            __conn = sqlite3.connect(self.__db_file)
            __c = __conn.cursor()
            count = 0
            #setup tables if necessary
            __c.execute('create table if not exists files (uri TEXT, mimetype TEXT, artist TEXT, album TEXT, title TEXT, tracknumber INTEGER, year INTEGER, genre TEXT, starred INTEGER DEFAULT 0, primary key(uri))')
            for root, ds, fs in os.walk(unicode(self.__index_dir), followlinks=True):
                if not self.__continue:
                    break
                for f in fs:
                    if not self.__continue:
                        break
                    filename = os.path.join(root, f)
                    mime, encoding = mimetypes.guess_type(filename)
                    if(mime != None and mime.find("audio") == 0 and mime != "audio/x-mpegurl"):
                        af = self.__wrapper.create_audio_file(filename)
                        self.__update_db(af, __c)

            __conn.commit()
            __c.close()


    def __del__(self):
        self.stop_thread()

    def stop_thread(self):
        self.__continue = False

    def __update_db(self, data, c):
        does_exist = u"select uri from files where uri='" + LocalIndexer.get_db_string(data.get_uri()) + u"'"
        c.execute(does_exist)
        row = c.fetchone()
        if row == None:
            query = u"insert into files (uri, album, artist, title, tracknumber, genre) values ('" + LocalIndexer.get_db_string(data.get_uri()) + u"', '" + LocalIndexer.get_db_string(data.get_album().get_name()) + u"', '" + LocalIndexer.get_db_string(data.get_artist().get_name()) + u"', '" + LocalIndexer.get_db_string(data.get_title()) + u"', " + unicode(str(data.get_tracknumber())) + u", '" + LocalIndexer.get_db_string(data.get_genre()) + u"')"
        else:
            query = u"update files set artist='" + LocalIndexer.get_db_string(data.get_artist().get_name()) + u"', album='" + LocalIndexer.get_db_string(data.get_album().get_name()) + u"', title='" + LocalIndexer.get_db_string(data.get_title()) + u"', tracknumber=" + str(data.get_tracknumber()) + u", genre='" + LocalIndexer.get_db_string(data.get_genre()) + u"' where uri='" + LocalIndexer.get_db_string(data.get_uri()) + u"'"
        c.execute(query)
    
    @staticmethod
    def get_db_string(s):
        return s.replace("'", "''")

    @staticmethod
    def from_db_string(s):
        return s.replace("''", "'")

