# -*- coding: utf-8 -*-

import spotipy.core.baseplaylist
import os
import mimetypes

class LocalPlayList(spotipy.core.baseplaylist.BasePlayList):
    def __init__(self, wrapper, d, name = None):
        spotipy.core.baseplaylist.BasePlayList.__init__(self, wrapper)
        self.__local_dir = d
        self.__name = name

    def get_name(self):
        if self.__name != None:
            return self.__name
        else:
            return os.path.basename(self.__local_dir)

    def get_tracks(self):
        ret = []
        playlist = self.__local_dir
        if os.path.exists(playlist):
            list = sorted(os.listdir(playlist))

            count = 0
            for f in list:
                filename = os.path.join(playlist, f)
                mime, encoding = mimetypes.guess_type(filename)
                if(mime != None and mime.find("audio") == 0 and mime != "audio/x-mpegurl"):
                    count += 1
                    af = self.get_wrapper().create_audio_file(filename)
                    af.set_index(count)
                    #yield af
                    ret.append(af)
        return ret
