# -*- coding: utf-8 -*-
from spotipy.core.album import Album
import spotifyartist
import spotify
import time

class SpotifyAlbum(Album):
    __instances = {}

    def __new__(cls, album, wrapper):
        uri = str(spotify.Link.from_album(album))
        if uri in cls.__instances:
            return cls.__instances[uri]
        else:
            inst = super(SpotifyAlbum, cls).__new__(cls, album, wrapper)
            cls.__instances[uri] = inst
            return inst

    def __init__(self, album, wrapper):
        Album.__init__(self, album.name(), "")
        self.__album = album
        self.__wrapper = wrapper
    def get_name(self):
        return self.__album.name()
    def get_uri(self):
        return str(spotify.Link.from_album(self.__album))
    def get_artist(self):
        return spotifyartist.SpotifyArtist(self.__album.artist(), self.__wrapper)

    def get_tracks(self):
        t = Album.get_tracks(self)
        if len(t) == 0:
            def cb(browse_data):
                pass
            browser = self.__wrapper.session.browse_album(self.__album, cb)
            while not browser.is_loaded():
                time.sleep(0.01)
            l = []
            for t in browser:
                l.append(self.__wrapper.create_audio_file(t))
            self.set_tracks(l)
            return l
        else:
            return t

    def get_type(self):
        type = self.__album.type()
        if spotify.Album.ALBUM == type:
            return Album.ALBUM
        elif spotify.Album.SINGLE == type:
            return Album.SINGLE
        elif spotify.Album.COMPILATION == type:
            return Album.COMPILATION
        else:
            return Album.UNKNOWN

    #def get_type(self):
    #    return self.__type
    def get_year(self):
        return self.__album.year()
    #def get_tracks(self):
    #    return self.__tracks
    #def set_tracks(self, value):
    #    del self.__tracks[:]
    #    self.__tracks.extend(value)

