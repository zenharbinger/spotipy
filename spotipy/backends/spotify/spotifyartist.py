# -*- coding: utf-8 -*-

from spotipy.core.artist import Artist
import spotifyalbum
import spotify
import time

class SpotifyArtist(Artist):
    __instances = {}

    def __new__(cls, artist, wrapper):
        uri = str(spotify.Link.from_artist(artist))
        if uri in cls.__instances:
            return cls.__instances[uri]
        else:
            inst = super(SpotifyArtist, cls).__new__(cls, artist, wrapper)
            cls.__instances[uri] = inst
            return inst
    
    def __init__(self, artist, wrapper):
        Artist.__init__(self, artist.name(), "")
        self.__artist = artist
        self.__wrapper = wrapper
    def get_name(self):
        return self.__artist.name()
    def get_uri(self):
        return str(spotify.Link.from_artist(self.__artist))
    def get_albums(self):
        def cb(browse_data):
            pass
        browser = self.__wrapper.session.browse_artist(self.__artist, cb)
        while not browser.is_loaded():
            time.sleep(0.01)
        l = []
        for t in browser:
            l.append(spotifyalbum.SpotifyAlbum(t, self.__wrapper))
        return l