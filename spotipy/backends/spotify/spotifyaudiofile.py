# -*- coding: utf-8 -*-

import os
import spotify  #for getting id3 tag info on audio data
from spotipy.core.audiofile import AudioFile
from spotifyartist import SpotifyArtist
from spotifyalbum import SpotifyAlbum

class SpotifyAudioFile(AudioFile):
    def __init__(self, track):
        AudioFile.__init__(self, track)
        self.__track = track
        self.set_artist(track.artists()[0].name())
        self.set_album(track.album().name())
        self.set_title(track.name())
        self.set_tracknumber(0)
        self.set_year(track.album().year())
        #self.set_starred(False)
        self.__similar = []

    def get_artist(self):
        return SpotifyArtist(self.__track.artists()[0], self.get_wrapper())
    def get_artists(self):
        return [self.get_artist()]
    def get_album(self):
        return SpotifyAlbum(self.__track.album(), self.get_wrapper())

    def _probably_equal(self, other):
        count = 0
        if self.get_artist() == other.get_artist():
            count += 1
        if self.get_title() == other.get_title():
            count += 1
        #if self.album == other.album:
        #    count += 1
        return count == 2

    #def get_gtk_icon_id(self):
    #    return "gtk-network"

    def add_similar(self, similar):
        self.__similar.append(similar)

    def get_similar(self):
        return self.__similar

    def get_album_art(self):
        #art = self.get_wrapper().session.image_create(self.__track.album().cover())
        return None

    def set_wrapper(self, value):
        self.starred = self.__track.starred(value.session)
        AudioFile.set_wrapper(self, value)

    def set_starred(self, value):
        self.__track.starred(self.get_wrapper().session, value)
        AudioFile.set_starred(self, value)

    def get_starred(self):
        return self.__track.starred(self.get_wrapper().session)

    def get_uri(self):
        return  str(spotify.Link.from_track(self.__track, 0))
