# -*- coding: utf-8 -*-

import spotipy.core.baseplaylist
import spotify

class SpotifyPlayList(spotipy.core.baseplaylist.BasePlayList):
    def __init__(self, wrapper, pl, name = None):
        spotipy.core.baseplaylist.BasePlayList.__init__(self, wrapper)
        self.__spotify_playlist = pl
        self.__name = name
        self.__listed = None

    def get_name(self):
        if self.__name != None:
            return self.__name
        else:
            return self.__spotify_playlist.name()

    def get_tracks(self):
        if self.__listed == None:
            self.__listed = []
            if self.__spotify_playlist != None:
                if self.__spotify_playlist.type() == "playlist":
                    for i, t in enumerate(self.__spotify_playlist):
                        af = self.get_wrapper().create_audio_file(t)
                        af.set_tracknumber(i + 1)
                        af.set_index(af.get_tracknumber())
                        self.__listed.append(af)
                        #yield af
                else:
                    for x in self.get_child_lists():
                        #for t in x.get_tracks():
                        #   yield t
                        self.__listed.extend(x.get_tracks())
        return self.__listed
