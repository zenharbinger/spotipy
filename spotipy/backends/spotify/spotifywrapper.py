# -*- coding: utf-8 -*-
import sys, traceback, time, threading, os, base64, logging

import spotify
import spotipy

from spotify.manager import SpotifySessionManager, SpotifyPlaylistManager, \
    SpotifyContainerManager
from spotipy.core.basewrapper import BaseWrapper
import spotifyplaylist
from spotipy.backends.local.gstreamerwrapper import GStreamerWrapper
from spotipy.backends.spotify.spotifyaudiofile import SpotifyAudioFile
from spotipy.backends.spotify.spotifyartist import SpotifyArtist
from spotipy.backends.spotify.spotifyalbum import SpotifyAlbum
from spotipy.core.album import Album
from spotipy.core.artist import Artist
from spotipy.core.result import Result

from spotify import Link, SpotifyError, ToplistBrowser

#perform multiple fall-backs on availability of sound controllers
try:
    from spotify.alsahelper import AlsaController
except ImportError:
    try:
        from spotify.osshelper import OssController as AlsaController
    except:
        logger = logging.getLogger("spotipy.backends.spotify.SpotifyWrapper")
        logger.warning("pythhon-alsaaudio or python-oss must be installed for Spotify")
        raise

## playlist callbacks ##
class DummyPlaylistManager(SpotifyPlaylistManager):
    def tracks_added(self, p, t, i, u):
        #print 'Tracks added to playlist %s' % p.name()
        pass

    def tracks_moved(self, p, t, i, u):
        #print 'Tracks moved in playlist %s' % p.name()
        pass

    def tracks_removed(self, p, t, u):
        #print 'Tracks removed from playlist %s' % p.name()
        pass

## container calllbacks ##
class DummyContainerManager(SpotifyContainerManager):
    def __init__(self, session_manager):
        self.loaded = False
        self.session_manager = session_manager

    def container_loaded(self, c, u):
        self.loaded = True
        self.session_manager.container_manager_loaded()

    def playlist_added(self, c, p, i, u):
        self.session_manager.playlist_manager.watch(p)
        p.add_playlist_state_changed_callback(self.session_manager.playlist_state_changed, u)

    def playlist_moved(self, c, p, oi, ni, u):
        pass

    def playlist_removed(self, c, p, i, u):
        pass

class SpotifyWrapper(BaseWrapper, SpotifySessionManager, threading.Thread):
    appkey_file = None
    cache_location = spotipy.CACHE_PATH
    settings_location = spotipy.SETTINGS_PATH
    application_key = base64.decodestring(spotipy.SPOTIFY_KEY)
    user_agent = 'spotipy'
    __now_playing = None
 
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(SpotifyWrapper, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    @classmethod
    def get_instance(cls):
        return cls._instance

    def __init__(self):
        threading.Thread.__init__(self)
        SpotifySessionManager.__init__(self, spotipy.settings.SPOTIFY_USERNAME, spotipy.settings.SPOTIFY_PASSWORD, True)
        self.__logger = logging.getLogger("spotipy.backends.spotify.SpotifyWrapper")
        self.__logger.info("SpotiPy uses SPOTIFY(R) CORE")
        self.__audio = AlsaController()
        self.ctr = None
        self.__status = spotipy.PlayState.NULL
        self.__watchers = []
        self.setDaemon(True)
        self.__track_end_listeners = []
        self.__md_counter = 0
        self.__md_count_to_ms_factor = 46.4
        self.__playlist_cb = {}
        self.__playlist_complete = False
        self.add_playlist_listener(self.__spotify_playlist_listener)
        self.__login()
        self.__local_files = GStreamerWrapper.get_instance()
        self.__audio_files = {}

    def __spotify_playlist_listener(self, sender):
        """ Called when a playlist is loaded """
        complete = True
        for i, pl in enumerate(sender.ctr):
            complete = complete and pl.is_loaded()
        if complete and self.__playlist_cb != None:
            self.__playlist_complete = True
            l = self.__playlist_cb
            self.__playlist_cb = {}
            for cb in l.keys():
                cb(self.__spotify_get_playlists(), l[cb])

    def get_playlists(self, cb, *data):
        if self.__playlist_complete:
            pass
        self.__playlist_cb[cb] = data

    def __spotify_get_playlists(self):
        spotify_node = spotifyplaylist.SpotifyPlayList(self, None, "Spotify")
        row = [spotify_node]

        for i, x in enumerate(self.ctr):
            if x.type() == "playlist":
                tmp_row = spotifyplaylist.SpotifyPlayList(self, x)
                tmp_row.set_parent_list(row[0])
                row[0].add_child_list(tmp_row)
            elif x.type() == "folder_start":
                tmp_row = spotifyplaylist.SpotifyPlayList(self, x)
                tmp_row.set_parent_list(row[0])
                row[0].add_child_list(tmp_row)
                row.insert(0, tmp_row)
            elif x.type() == "folder_end":
                row.pop(0)

        return [spotify_node]

    def add_playlist_listener(self, listener):
        self.__watchers.append(listener)
        pass

    def add_track_listener(self, listener):
        self.__track_end_listeners.append(listener)

    def __login(self):
        self.playlist_manager = DummyPlaylistManager()
        self.container_manager = DummyContainerManager(self)
        self.__logger.info("Logging in to Spotify")

    def run(self):
        try:
            self.connect()
        except SpotifyError as ex:
            self.__logger.warning("SpotifyError: %s", str(ex))

    def playlist_state_changed(self, p, u):
        if p.is_loaded():
            for x in self.__watchers:
                x(self)

    def get_starred(self):
        return spotifyplaylist.SpotifyPlayList(self, self.starred, "Starred")

    def container_manager_loaded(self):
        for x in self.__watchers:
            x(self)

    def logged_in(self, session, error):
        if error:
            self.__logger.error("%s", error)
            return
        self.session = session
        if spotipy.settings.SPOTIFY_BITRATE == 160:
            self.__logger.info("Spotify Bitrate: %s", "160")
            self.set_preferred_bitrate(0)
        elif spotipy.settings.SPOTIFY_BITRATE == 320:
            self.__logger.info("Spotify Bitrate: %s", "320")
            self.set_preferred_bitrate(1)
        else:
            self.__logger.info("Spotify Bitrate: %s", "96")
            self.set_preferred_bitrate(2)
        try:
            #print "Logged In..."
            self.__logger.debug("Logged in to Spotify")
            self.ctr = session.playlist_container()
            self.container_manager.watch(self.ctr)
            self.starred = session.starred()
        except Exception as ex:
            self.__logger.error("Spotify Login Error: %s", str(ex))
            #traceback.print_exc()

    def logged_out(self, session):
        pass

    def do_search(self, query, callback = None):
        self.__search_cb = callback
        self.session.search(query, self.__search_callback)
        return []

    def __search_callback(self, results, user):
        r = Result()
        try:
            if self.__search_cb != None:
                count = 1
                l = []
                for track in results.tracks():
                    af = self.create_audio_file(track)
                    af.set_index(count)
                    af.set_tracknumber(count)
                    count += 1
                    af.set_wrapper(self)
                    insert = True
                    for x in l:
                        if x._probably_equal(af):
                            x.add_similar(af)
                            insert = False
                            break
                    if insert:
                        l.append(af)
                
                r.set_tracks(l)
                #print r.get_tracks()
                r.set_query(results.query())
                r.set_suggestion(results.did_you_mean())
                
                artist_list = []
                for artist in results.artists():
                    a = SpotifyArtist(artist, self)
                    artist_list.append(a)
                r.set_artists(artist_list)

                album_list = []
                for album in results.albums():
                    a = SpotifyAlbum(album, self)
                    album_list.append(a)
                r.set_albums(album_list)

                self.__search_cb(self, r)
        except:
            raise
            self.__search_cb(self, r)
        return

    def __load_track(self, track):
        if self.__status == spotipy.PlayState.PLAYING:
            self.stop()
        self.session.load(track)
        self.__md_counter = 0
        self.__now_playing = track

    def pause(self):
        if self.__status == spotipy.PlayState.PAUSED:
            self.session.play(1)
            self.__status = spotipy.PlayState.PLAYING
        elif self.__status == spotipy.PlayState.PLAYING:
            self.session.play(0)
            self.__status = spotipy.PlayState.PAUSED
        return self.__status

    def get_username(self):
        return self.session.username()

    def play(self, track):
        if type(track) == str or type(track) == unicode:
            track = spotify.Link.from_string(track).as_track()

        l = self.__local_files.find_tracks(track.album().name(), track.artists()[0].name(), track.name())
        if len(l) > 0:
            self.__logger.info("Playing Local Track: %s", l[0].get_uri())
            return l[0].play()

        self.__load_track(track)
        self.session.play(1)
        self.__status = spotipy.PlayState.PLAYING
        return self.create_audio_file(track)

    def stop(self):
        self.session.play(0)
        self.__status = spotipy.PlayState.STOPPED
        self.__now_playing = None

    def get_position(self):
        if self.__now_playing != None:
            return self.__md_counter * self.__md_count_to_ms_factor
        else:
            return -1

    def __seek(self, seconds):
        pass

    position = property(get_position)

    def is_playing(self):
        return self.__now_playing != None

    def get_duration(self):
        if self.__now_playing != None:
            return self.__now_playing.duration()
        return -1

    duration = property(get_duration)

    def get_norm_position(self):
        if self.__now_playing != None:
            ret =  float(self.position) / float(self.duration)
            return ret * float(100.0)
        else:
            return -1

    def set_norm_position(self, percent):
        if self.__status == spotipy.PlayState.PLAYING:
            try:
                factor = (float(100.0) / percent)
                f_value = float(self.duration) / factor
                value = int(f_value)
            except ZeroDivisionError:
                f_value = 0
                #this will happen if seeking back to the beginning of the track.
                value = 0
            except:
                #this will probably happen when we are seeking to the end of the track.
                value = -1
            if value >= 0:
                try:
                    self.session.play(0)
                    self.session.seek(value)
                    self.session.play(1)
                    self.__md_counter = int(f_value / self.__md_count_to_ms_factor)
                except Exception as ex:
                    self.__logger.warning("Error Playing: %s", str(ex))

    norm_position = property(get_norm_position, set_norm_position)

    def music_delivery(self, *a, **kw):
        self.__md_counter = self.__md_counter + 1
        return self.__audio.music_delivery(*a, **kw)

    def end_of_track(self, sess):
        try:
            for listener in self.__track_end_listeners:
                listener(self, None)
        except Exception as ex:
            self.__logger.warning("Error In Listener: %s", str(ex))
    def set_preferred_bitrate(self, value):
        if value == 96:
            value = 2
        elif value == 160:
            value = 0
        elif value == 320:
            value = 1
        try:
            if self.session != None:
                self.session.set_preferred_bitrate(value)
        except:
            pass

    #def browse(self, link, callback):
    #    if link.type() == link.LINK_ALBUM:
    #        browser = self.session.browse_album(link.as_album(), callback)
    #        while not browser.is_loaded():
    #            time.sleep(0.1)
    #        for track in browser:
    #            print track
    #    if link.type() == link.LINK_ARTIST:
    #        browser = self.session.browse_artist(link.as_artist(), callback)
    #        while not browser.is_loaded():
    #            time.sleep(0.1)
    #        for album in browser:
    #            print album.name()
    #    callback(browser)

    def watch(self, p, unwatch=False):
        if not unwatch:
            self.__logger.debug("Watching playlist: %s", p.name())
            self.playlist_manager.watch(p);
        else:
            self.__logger.debug("Unwatching playlist: %s", p.name())
            self.playlist_manager.unwatch(p)

    #def toplist(self, tl_type, tl_region):
    #    print repr(tl_type)
    #    print repr(tl_region)
    #    def callback(tb, ud):
    #        for i in xrange(len(tb)):
    #            print '%3d: %s' % (i+1, tb[i].name())
    #    tb = ToplistBrowser(tl_type, tl_region, callback)

    def shutdown(self):
        self.stop()
        self.session.logout()

    def create_audio_file(self, data):
        key = str(spotify.Link.from_track(data, 0))
        if not key in self.__audio_files.keys():
            import spotifyaudiofile
            ret = spotifyaudiofile.SpotifyAudioFile(data)
            ret.set_wrapper(self)
            self.__audio_files[key] = ret
            return ret
        else:
            return self.__audio_files[key]

