# -*- coding: utf-8 -*-

import sys
try:
     import gi
     gi.require_version('Gdk', '3.0')
except:
      pass
try:
    from gi.repository import Gdk, GObject
except:
    sys.exit(1)

def main():
    GObject.threads_init()

    from spotipy.player import Player
    app = Player() 
    Gdk.threads_enter()
    app.run(None)
    Gdk.threads_leave()

if __name__ == "__main__":
    main()
