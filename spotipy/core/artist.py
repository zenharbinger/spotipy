# -*- coding: utf-8 -*-

class Artist(object):
    def __init__(self, name, uri):
        self.__name = name
        self.__uri = uri
        self.__tracks = []
        self.__albums = []
    def get_name(self):
        return self.__name
    def get_uri(self):
        return self.__uri
    def get_albums(self):
        return self.__albums
    def set_albums(self, value):
        del self.__albums[:]
        self.__albums.extend(value)
    def get_tracks(self):
        return self.__tracks
    def set_tracks(self, value):
        del self.__tracks[:]
        self.__tracks.extend(value)

