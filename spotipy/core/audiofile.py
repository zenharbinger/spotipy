# -*- coding: utf-8 -*-

class AudioFile:
    def __init__(self, filename):
        self.__album = "Unknown"
        self.__artist = "Unknown"
        self.__genre = "Unknown"
        self.__title = filename
        self.__year = 0
        self.__tracknumber = 0
        self.__starred = False
        self.__wrapper = None
        self.__index = 0
        self.__list = None
        #self.__gtk_icon = "gtk-file"

    def __str__(self):
        return self.get_uri()

    def __hash__(self):
        return hash(self.get_uri())
    def __eq__(self, other):
        return self.get_uri() == other.get_uri()

    def __cmp__(self, other):
        if other == None:
            return 1
        if self.__index < other.get_index():
            return -1
        elif self.__index > other.get_index():
            return 1
        else:
            return 0
    def get_name(self):
        return self.get_title()

    def get_album(self):
        return self.__album
    def set_album(self, value):
        self.__album = value
        self.__update_list(1, value)
    def get_artist(self):
        return self.__artist
    def get_artists(self):
        return [self.get_artist()]
    def set_artist(self, value):
        self.__artist = value
        self.__update_list(0, value)
    def get_genre(self):
        return self.__genre
    def set_genre(self, value):
        self.__genre = value
    def get_title(self):
        return self.__title
    def set_title(self, value):
        self.__title = value
        self.__update_list(2, value)
    def get_year(self):
        return self.__year
    def set_year(self, value):
        self.__year = int(value)
        self.__update_list(5, value)
    def get_tracknumber(self):
        return self.__tracknumber
    def set_tracknumber(self, value):
        self.__tracknumber = int(value)
        self.__update_list(4, value)
    def get_index(self):
        return self.__index
    def set_index(self, value):
        self.__index = int(value)
    def get_wrapper(self):
        if self.__wrapper == None:
            raise Exception("Null Wrapper")
        return self.__wrapper
    def set_wrapper(self, value):
        if value != None:
            self.__wrapper = value

    def add_similar(self, similar):
        pass

    def get_similar(self):
        return []

    def play(self):
        if self.__wrapper != None:
            #self.set_gtk_icon_id("gtk-play")
            #print self.__gtk_icon
            #print self.__list[8]
            return self.__wrapper.play(self.get_uri())
        return None

    def get_album_art(self):
        return None

    def set_starred(self, value):
        self.__starred = value
        self.__update_list(6, value)
        pass

    def get_starred(self):
        return self.__starred

    def pause(self):
        if self.wrapper != None:
            return self.__wrapper.pause()
        return False

    def get_uri(self):
        return ""

    #def get_gtk_icon_id(self):
    #    return self.__gtk_icon

    #def set_gtk_icon_id(self, value):
    #    self.__update_list(8, value)
    #    print "in set " + value
    #    self.__gtk_icon = value

    def __update_list(self, index, value):
        if self.__list == None:
            return
        else:
            self.__list[index] = value        

    def to_list(self):
        if self.__list == None:
            self.__list = [self.get_artist(), self.get_album(), self.get_title(), "", self.get_tracknumber(), self.get_year(), self.get_starred(), self.get_uri(), self.get_gtk_icon_id()]
        return self.__list
