# -*- coding: utf-8 -*-

class BasePlayList(object):
    #_tracks = []
        
    def get_name():
        return ""

    def __init__(self, wrapper):
        self.__wrapper = None
        self._parent_list = None
        self._child_lists = []
        self.__uri = None
        self.__wrapper = wrapper

    def get_wrapper(self):
        return self.__wrapper;

    def add_child_list(self, l):
        if not l in self._child_lists:
            self._child_lists.append(l)

    def remove_child_list(self, l):
        if l in self._child_lists:
            self._child_lists.remove(l)

    def get_child_lists(self):
        return self._child_lists

    def set_parent_list(self, p):
        self._parent_list = p

    def get_parent_list(self):
        return self._parent_list

    def get_tracks(self):
        return []
