# -*- coding: utf-8 -*-

from result import Result

class BaseWrapper(object):
    def play(self, track):
        pass

    def stop(self):
        pass

    def pause(self):
        return False

    def set_norm_position(self, pos):
        """Normalized Position, integer values of percent from 0 to 100"""
        pass

    def get_norm_position(self):
        """Normalized Position, integer values of percent from 0 to 100"""
        return -1

    def get_starred(self):
        return None

    def get_position(self):
        """Absolute Position in track in milliseconds"""
        return -1

    def get_duration(self):
        """Duration of loaded song in milliseconds"""
        return -1
    def get_username(self):
        return ""

    def cansave(self):
        return False

    def is_playing(self):
        return False

    def add_playlist_listener(self, listener):
        pass

    def add_track_listener(self, listener):
        pass

    def shutdown(self):
        pass

    def do_search(self, query, result_callback=None):
        r = Result()
        r.set_query(query)
        if result_callback != None:
            result_callback(self, r)
        return r

    def get_playlists(self, cb, *data):
        if cb != None:
            cb([], data)

    def create_audio_file(self, data):
        return None
    
