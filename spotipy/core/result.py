# -*- coding: utf-8 -*-

class Result(object):
    def __init__(self, tracks=[]):
        self.__tracks = []
        self.__tracks.extend(tracks)
        self.__albums = []
        self.__artists = []
        self.__query = None
        self.__suggestion = None
    def get_tracks(self):
        return self.__tracks
    def get_albums(self):
        return self.__albums
    def get_artists(self):
        return self.__artists
    def get_suggestion(self):
        return self.__suggestion
    def get_query(self):
        return self.__query

    def set_tracks(self, value):
        del self.__tracks[:]
        self.__tracks.extend(value)
    def set_albums(self, value):
        del self.__albums[:]
        self.__albums.extend(value)
    def set_artists(self, value):
        del self.__artists[:]
        self.__artists.extend(value)
    def set_suggestion(self, value):
        self.__suggestion = value
    def set_query(self, value):
        self.__query = value
