import pylast
from pykka.actor import ThreadingActor
#from pykka.registry import ActorRegistry
import time, logging
import spotipy
import spotipy.settings as settings
import urllib2, hashlib
import os, sys, traceback

from spotipy.listeners import BackendListener

#keys for spotipy
API_KEY = ''
API_SECRET = ''

class LastfmFrontend(ThreadingActor, BackendListener):
    """
    Frontend which scrobbles the music you play to your `Last.fm
    <http://www.last.fm>`_ profile.

    .. note::

        This frontend requires a free user account at Last.fm.

    **Dependencies:**

    - `pylast <http://code.google.com/p/pylast/>`_ >= 0.5.7

    **Settings:**

    - :attr:`spotipy.settings.LASTFM_USERNAME`
    - :attr:`spotipy.settings.LASTFM_PASSWORD`
    """

    #_instance = None

    #def __new__(cls, *args, **kwargs):
    #    if not cls._instance:
    #        cls._instance = super(LastfmFrontend, cls).__new__(
    #                            cls, *args, **kwargs)
    #    return cls._instance

    #@classmethod
    #def get_instance(cls):
    #    return cls._instance

    def __init__(self):
        self.__logger = logging.getLogger("spotipy.LastfmFrontend")
        self.lastfm = None
        self.last_start_time = None
        self.login(settings.LASTFM_USERNAME, settings.LASTFM_PASSWORD)

    def get_image(self, track, cb):
        try:
            album = self.lastfm.get_album(track.get_artist().get_name(), track.get_album().get_name())
            uri = album.get_cover_image(size=3)

            if uri == None:
                artist = self.lastfm.get_artist(track.get_artist().get_name())
                uri = artist.get_cover_image(size=3)

            m = hashlib.md5()
            h = hashlib.sha224(uri).hexdigest()

            path = os.path.join(spotipy.CACHE_PATH, "images")
            if not os.path.exists(path):
                os.mkdir(path)

            path = os.path.join(path, h + ".jpg")

            if os.path.isfile(path):
                pass
                #print "exists"
            else:
                img_file = urllib2.urlopen(uri)
                output = open(path,'wb')
                output.write(img_file.read())
                output.close()
            #print uri
            #print path
            cb(path)
            return
        except:
            traceback.print_exc(file=sys.stderr)
        cb(none)
        return

    def get_bio_content(self, track, cb):
        try:
            artist = self.lastfm.get_artist(track.get_artist().get_name())
            cb(artist.get_bio_content())
            return
        except:
            traceback.print_exc(file=sys.stderr)
        cb(None)
        return

    def get_album_bio_content(self, track, cb):
        try:
            artist = self.lastfm.get_album(track.get_artist().get_name(), track.get_album().get_artist().get_name())
            cb(artist.get_wiki_content())
            return
        except:
            traceback.print_exc(file=sys.stderr)
        cb(None)
        return

    def login(self, username, password):
        try:
            password_hash = pylast.md5(password)
            self.lastfm = pylast.LastFMNetwork(
                api_key=API_KEY, api_secret=API_SECRET,
                username=username, password_hash=password_hash)
            self.__logger.info(u'Connected to Last.fm')
        except (pylast.NetworkError, pylast.MalformedResponseError,
                pylast.WSError) as e:
            self.lastfm = None
            self.__logger.error(u'Error during Last.fm setup: %s', e)

    def track_playback_started(self, track):
        if self.lastfm == None:
            return
        artists = ', '.join([a.get_name() for a in track.get_artists()])
        duration =  track.get_wrapper().get_duration() // 1000
        self.last_start_time = int(time.time())
        self.__logger.debug(u'Now playing track: %s - %s', artists, track.get_name())
        try:
            self.lastfm.update_now_playing(
                artists,
                (track.get_name() or ''),
                album=(track.get_album().get_name() or ''),
                duration=str(duration),
                track_number=str(track.get_tracknumber()),
                mbid='')
        except (pylast.ScrobblingError, pylast.NetworkError,
                pylast.MalformedResponseError, pylast.WSError) as e:
            self.__logger.warning(u'Error submitting playing track to Last.fm: %s', e)

    def track_playback_ended(self, track, duration, time_position):
        if self.lastfm == None:
            return
        artists = ', '.join([a for a in track.get_artists()])
        duration = duration // 1000
        time_position = time_position // 1000

        if duration < 30:
            self.__logger.debug(u'Track too short to scrobble. (30s)')
            return
        if time_position < duration // 2 and time_position < 240:
            self.__logger.debug(u'Track not played long enough to scrobble. (50% or 240s)')
            return
        if self.last_start_time is None:
            self.last_start_time = int(time.time()) - duration
        self.__logger.debug(u'Scrobbling track: %s - %s', artists, track.get_name())
        try:
            self.lastfm.scrobble(
                artists,
                (track.get_name() or ''),
                str(self.last_start_time),
                album=(track.get_album().get_name() or ''),
                track_number=str(track.get_tracknumber()),
                duration=str(duration),
                mbid='')
        except (pylast.ScrobblingError, pylast.NetworkError,
                pylast.MalformedResponseError, pylast.WSError) as e:
            self.__logger.warning(u'Error submitting played track to Last.fm: %s', e)

