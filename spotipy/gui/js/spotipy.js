function stopped()
{
    var elems = document.body.getElementsByTagName("tr");
    var index = 0;
    for(index = 0; index < elems.length; index++)
    {
        elems[index].setAttribute("class", "");
    }
}

function playing(id)
{
    stopped();
    x = document.getElementById(id);
    if (x != null)
    {
        x.setAttribute("class", "playing");
        x.scrollIntoView(false);
    }
}

