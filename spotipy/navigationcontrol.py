# -*- coding: utf-8 -*-

class NavigationType(object):
    UKNOWN = 0
    PLAYLIST = 1
    ALBUM = 2
    ARTIST = 3
    SEARCH = 4

class NavigationCommand(object):
    def __init__(self, uri, tracks=[], playlist=None, html=""):
        self.__uri = uri
        self.__playlist = playlist
        self.__html = html
        self.__type = NavigationType.UKNOWN
        self.__tracks = []
        self.__tracks.extend(tracks)

        if uri.find("artist://") == 0:
            self.__type = NavigationType.ARTIST
        elif uri.find("album://") == 0:
            self.__type = NavigationType.ALBUM
        elif uri.find("playlist://") == 0:
            self.__type = NavigationType.PLAYLIST
        elif uri.find("search://") == 0:
            self.__type = NavigationType.SEARCH

    def __str__(self):
        return self.get_uri()

    def get_tracks(self):
        return self.__tracks
    def get_type(self):
        return self.__type
    def get_uri(self):
        return self.__uri
    def get_playlist(self):
        return self.__playlist
    def get_html(self):
        return self.__html
    def get_type(self):
        return self.__type

class NavigationControl(object):
    def __init__(self):
        self.__curr_index = 0
        self.__history = []

    def __len__(self):
        return len(self.__history)

    def add_new(self, command):
        del self.__history[self.__curr_index + 1:]
        self.__history.append(command)
        self.__curr_index = len(self.__history) - 1
        return command

    def get_prev(self):
        if self.can_get_prev():
            self.__curr_index -= 1
        return self.__history[self.__curr_index]

    def get_next(self):
        if self.can_get_next():
            self.__curr_index += 1
        return self.__history[self.__curr_index]

    def can_get_next(self):
        return self.__curr_index < len(self.__history) - 1
    def can_get_prev(self):
        return self.__curr_index > 0

