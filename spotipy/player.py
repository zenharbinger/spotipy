# -*- coding: utf-8 -*-

import sys, os, threading, codecs, logging, getpass
#import glib

import gi
try:
    gi.require_version('Gtk', '3.0')
    gi.require_version('WebKit', '3.0') 
    from gi.repository import Gtk, Gdk, GObject, GdkPixbuf, Gio, WebKit
except:
    sys.exit(1)

try:
    gi.require_version('AppIndicator3', '0.1')
    from gi.repository import AppIndicator3
except:
    AppIndicator3 = None
try:
    gi.require_version('Notify', '0.7')
    from gi.repository import Notify
except:
    Notify = None

#import pyinotify    #used for monitoring specified directories for changes
import spotipy
from spotipy.core.album import Album
from spotipy.queuecontrol import QueueControl
from spotipy.navigationcontrol import NavigationControl
from spotipy.navigationcontrol import NavigationCommand
#import spotipy.core.basewrapper

#import spotipy.core.baseplaylist
try:
    from spotipy.frontends.lastfm import LastfmFrontend
except:
    LastfmFrontend = None
try:
    from spotipy.frontends.mpris import MprisFrontend
except:
    MprisFrontend = None
try:
    from spotipy.backends.spotify.spotifywrapper import SpotifyWrapper
except:
    SpotifyWrapper = None
from spotipy.core.result import Result

#These don't seem to be necessary for the plugin code below to work.
#import re
#sys.path.append(os.path.join(os.path.abspath(os.path.dirname(os.path.abspath( __file__ )))))
#sys.path.append(os.path.join(os.path.abspath(os.path.dirname(os.path.abspath( __file__ ))), 'plugins'))

class PlayControl(object):
    def stop(self):
        pass
    def play(self):
        pass
    def playpause(self):
        pass
    def next(self):
        pass
    def prev(self):
        pass
    def repeat(self):
        pass
    def get_shuffle(self):
        pass
    def set_shuffle(self, value):
        pass
    def seek(self, offset):
        pass
    def openuri(self, uri):
        pass
    def setposition(self, track, pos):
        pass
    #def play(self):
    #    pass

class Player(Gtk.Application, PlayControl):
    HISTORY = "History"
    QUEUE = "Queue"
    SEARCH_RESULTS = "Search Results"
    STARRED = "Starred"

    #_instance = None
    #def __new__(cls, *args, **kwargs):
    #    if not cls._instance:
    #        cls._instance = super(Player, cls).__new__(
    #                            cls, *args, **kwargs)
    #    return cls._instance

    #@classmethod
    #def get_instance(cls):
    #    return cls._instance

    def __init__(self):
        """ Constructor """
        Gtk.Application.__init__(self, application_id="apps.spotipy",
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.connect("activate", self.__on_activate)
        spotipy.settings._PLAYER = self

    def search_icon_press(self, pos, event, data):
        if event == Gtk.EntryIconPosition.SECONDARY:
            pos.set_text("")

    def on_find(self, widget):
        del self.__search_result_list[:]
        if widget.get_text().strip() == "":
            return
        self.__on_find_helper(widget.get_text().strip())

    def __on_find_helper(self, query):
        del self.__search_result_list[:]
        self.__ret_count = 0
        for x in self.__wrappers:
            x.do_search(query, self.__on_find_callback)
        
        self.builder.get_object('spinner1').start()
        self.builder.get_object('spinner1').set_visible(True)

    def __on_find_callback(self, sender, results):
        GObject.idle_add(self.__on_find_callback2, sender, results)

    def __on_find_callback2(self, sender, results):
        self.__search_result_list.append(results)

        self.__ret_count += 1
        if self.__ret_count == len(self.__wrappers):
            self.builder.get_object('spinner1').stop()
            self.builder.get_object('spinner1').set_visible(False)

            name = ""
            try:
                (tv1, tv2) = self.__get_path_pair()
                iter1 = self.treestore.get_iter(tv2)
                name = self.treestore.get_value(iter1, 0)
            except:
                pass

            if name != self.SEARCH_RESULTS:
                #this will implicitly call fill_tracks w/ the SEARCH_RESULTS.
                treeview = self.builder.get_object ("treeview2")
                selection = treeview.get_selection()
                selection.select_iter(self.search_node)
            else:
                self.__build_search_html(self.__search_result_list)

    def __build_search_html(self, result_list):
        f = [self. __create_suggestions_table, self.__create_artists_table, self.__create_albums_table, self.__create_tracks_table]
        html = self.__create_html(result_list, f)
        self.__nav_control.add_new(NavigationCommand("search://", self.__current_list, None, html))
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)


    def __on_activate(self, data=None):
        #PLUGIN_DIRECTORY = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath( __file__ ))), 'plugins')
        #self.__import_plugins(PLUGIN_DIRECTORY)
        #PLUGIN_DIRECTORY = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath( __file__ ))), '')
        #self.__import_plugins(PLUGIN_DIRECTORY)
        #print basewrapper.BaseWrapper.__subclasses__()
        if Notify != None:
            Notify.init("SpotiPy")

        self.__logger = logging.getLogger("spotipy.SpotiPy")
        if LastfmFrontend != None:
            self.__lastfm = LastfmFrontend.start().proxy()
        else:
            self.__lastfm = None
        if MprisFrontend != None:
            self.__mpris = MprisFrontend.start().proxy()
        else:
            self.__mpris = None
        self.__curr_track = None
        self.__curr_track_selected = None
        self.__search_result_list = []
        self.__active_wrapper = None
        self.__wrappers = []
        self.__play_control = QueueControl()
        self.__nav_control = NavigationControl()
        self.__closing = False
        self.__lookup = {}
        self.__current_list = []

        #INIT GUI
        path = os.path.realpath(__file__)
        script_dir = os.path.dirname(path)
        logo_file = os.path.join(script_dir, "gui/spotipy.png")
        self.__logo = GdkPixbuf.Pixbuf.new_from_file(logo_file)
        self.__webkit_artist_bio = WebKit.WebView()
        settings = self.__webkit_artist_bio.get_settings()
        settings.set_property("enable-developer-extras", False)
        self.__webkit_artist_bio.connect("navigation-policy-decision-requested", self.__nav_request)
        settings.set_property("enable-default-context-menu", False)
        self.__webkit_artist_bio.set_full_content_zoom(True)

        self.__webkit_track_list = WebKit.WebView()
        settings = self.__webkit_track_list.get_settings()
        settings.set_property("enable-developer-extras", False)
        self.__webkit_track_list.connect("navigation-policy-decision-requested", self.__spotipy_nav_request)
        settings.set_property("enable-default-context-menu", False)
        self.__webkit_track_list.set_full_content_zoom(True)
        self.__webkit_track_list.connect("title-changed", self.__webkit_msg)
        self.__webkit_track_list.connect("notify::load-status", self.__webkit_load_status)

        self.__logger.debug("INIT GUI")
        path = os.path.realpath(__file__)
        script_dir = os.path.dirname(path)
        self.builder = Gtk.Builder()
        uifile = os.path.join(script_dir, "gui/spotipy.ui")
        self.builder.add_from_file(uifile)
        self.builder.connect_signals(self)
        self.builder.get_object("bio_box").add(self.__webkit_artist_bio)
        self.__webkit_artist_bio.show()

        self.builder.get_object("track_box").add(self.__webkit_track_list)
        self.__webkit_track_list.show()

        window = self.builder.get_object ("MainWindow")
        self.treestore = self.builder.get_object ("treestore1")

        treeview = self.builder.get_object ("treeview2")
        selection = treeview.get_selection()
        selection.connect('changed', self.fill_tracks)

        self.builder.get_object('spinner1').set_visible(False)
        self.builder.get_object('scale1').set_sensitive(False)
        scale = self.builder.get_object('scale1')
        scale.set_value(0)
        time_label = self.builder.get_object('curr_time')
        time_label.set_text("0:00")

        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

        self.status_context = self.builder.get_object('statusbar1').get_context_id(__file__)
        self.__status_bar_count = 0
        self.builder.get_object('statusbar1').push(self.status_context, "SpotiPy: " + spotipy.__version__)
        self.builder.get_object ("user_name_label").set_text(getpass.getuser())
        self.history_node = self.treestore.append(None, [self.HISTORY, None])
        self.queue_node = self.treestore.append(None, [self.QUEUE, None])
        self.search_node = self.treestore.append(None, [self.SEARCH_RESULTS, None])
        self.starred_node = self.treestore.append(None, [self.STARRED, None])

        html = ["<span style='text-align: center'><h1>Welcome to SpotiPy</h1></span>"]
        html.append("<img style='display: block; margin-left: auto; margin-right: auto;' src='http://spotipy.tros.org/spotipy.png'/>")
        html.append("<span style='text-align: center'><h2>Version: " + spotipy.get_version() + "</h2></span>")
        html = ''.join(html)
        #html = self.__create_html([Result(self.__current_list)], [self.__create_tracks_table])
        self.__nav_control.add_new(NavigationCommand(u"playlist://", self.__current_list, None, html))
        self.__webkit_track_list.load_string(html, "text/html", "utf-8", "")

        if Notify != None:
            self.__notify = Notify.Notification.new ("", "", None)
            self.__notify.set_icon_from_pixbuf(self.__logo)
            self.__notify.set_timeout(Notify.EXPIRES_DEFAULT)
            self.__notify.set_urgency(Notify.Urgency.NORMAL)
        else:
            self.__notify = None

        window.set_default_size (780, 580)
        window.show ()
        self.add_window(window)

        self.__populate_application_indicator()

        #INIT WRAPPERS
        self.__logger.debug("INIT WRAPPERS")

        #INIT GSTREAMER
        try:
            self.__logger.debug("INIT GSTREAMER")
            from spotipy.backends.local.gstreamerwrapper import GStreamerWrapper
            gstreamer = GStreamerWrapper()
            gstreamer.get_playlists(self.__playlist_cb)
            gstreamer.add_track_listener(self.__track_ended_listener)

            gstreamer.start()
            self.__wrappers.append(gstreamer)
        except Exception as ex:
            raise
            self.__logger.warning("GStreamer could not be initialized: %s", str(ex))
        try:
            #INIT SPOTIFY
            self.__logger.debug("INIT SPOTIFY")
            if SpotifyWrapper != None:
                if spotipy.settings.SPOTIFY_USERNAME == None or spotipy.settings.SPOTIFY_USERNAME == "":
                    self.on_spotify_cred(None)
    
                if spotipy.settings.SPOTIFY_USERNAME != None and spotipy.settings.SPOTIFY_USERNAME != "":
                    spotify_wrapper = SpotifyWrapper()
                    spotify_wrapper.get_playlists(self.__playlist_cb)
                    spotify_wrapper.add_track_listener(self.__track_ended_listener)
    
                    spotify_wrapper.start()
                    self.__wrappers.append(spotify_wrapper)
            else:
                self.__logger.warning("pyspotify not imported, Spotify not Initialized.")
        except Exception as ex:
            raise
            self.__logger.warning("Spotify could not be initialized: %s", str(ex))
        
        spotipy.settings.save_values()
        #SET CALLBACK FOR UPDATING SCALE/PROGRESS BAR
        GObject.timeout_add(250, self.__scale_timer)

    def on_navigate_back(self, widget):
        cmd = self.__nav_control.get_prev()
        del self.__current_list[:]
        self.__current_list.extend(cmd.get_tracks())
        self.__webkit_track_list.load_string(cmd.get_html(), "text/html", "utf-8", "")
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

    def on_navigate_forward(self, widget):
        cmd = self.__nav_control.get_next()
        del self.__current_list[:]
        self.__current_list.extend(cmd.get_tracks())
        self.__webkit_track_list.load_string(cmd.get_html(), "text/html", "utf-8", "")
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

    def __populate_application_indicator(self):
        if AppIndicator3 != None:
            self.__ind = AppIndicator3.Indicator.new(
                "example-simple-client",
                "indicator-messages",
                AppIndicator3.IndicatorCategory.APPLICATION_STATUS)
            self.__ind.set_status (AppIndicator3.IndicatorStatus.ACTIVE)
            self.__ind.set_icon("spotipy")

            # create a menu
            menu = Gtk.Menu()
            image = Gtk.ImageMenuItem(Gtk.STOCK_QUIT)
            image.set_use_stock(True)
            image.connect("activate", self.quit)
            image.show()
            menu.append(image)
            image = Gtk.ImageMenuItem(Gtk.STOCK_MEDIA_NEXT)
            image.set_use_stock(True)
            image.connect("activate", self.on_next)
            image.show()
            menu.append(image)
            check = Gtk.CheckMenuItem("Shuffle")
            check.connect("toggled", self.on_shuffle_menu)
            check.show()
            menu.append(check)
            check = Gtk.CheckMenuItem("Repeat")
            check.connect("toggled", self.on_repeat_menu)
            check.show()
            menu.append(check)

            menu.show()
            self.__ind.set_menu(menu)

    def __webkit_load_status(self, view, status):
        status = view.get_load_status()
        if status == WebKit.LoadStatus.FINISHED and self.__curr_track_selected != None:
            self.__webkit_track_list.execute_script("playing('" + self.__curr_track_selected.get_uri() + "')")

    def __rec_pl_walker2(self, l, tn):
        """ Recursively walks a directory tree to populate the treeview """
        for child in l:
            file_node = self.treestore.append(tn[0], [child.get_name(), child])
            tn.insert(0, file_node)
            self.__rec_pl_walker2(child.get_child_lists(), tn)
            tn.pop(0)

    def __rec_pl_walker(self, l):
        file_node = self.treestore.append(None, [l[0].get_name(), l[0]])
        ll = [file_node]
        self.__rec_pl_walker2(l[0].get_child_lists(), ll)

    def __playlist_cb(self, l, *data):
        GObject.idle_add(self.__rec_pl_walker, l)

    def __scale_timer(self):
        if self.__closing:
            return False

        try:
            scale = self.builder.get_object('scale1')
            time_label = self.builder.get_object('curr_time')
            if self.__curr_track != None:
                time_label.set_text(self.__time_to_str(int(self.__get_position())))
            else:
                time_label.set_text("0:00")
            if self.__active_wrapper != None:
                pos = float(self.__active_wrapper.get_norm_position())
                if pos >= 0:
                    scale.set_value(pos)
        except Exception as ex:
            print(str(ex))
            pass
        try:
            time_label = self.builder.get_object('duration_time')
            duration = -1
            if self.__active_wrapper != None:
                duration = int(self.__active_wrapper.get_duration())
            if duration >= 0:
                time_label.set_text(self.__time_to_str(duration))
            else:
                time_label.set_text("")
        except Exception as ex:
            time_label.set_text("")

        return True

    def __track_ended_listener(self, sender, track):
        self.__logger.debug("Track Ended: %s", str(track))
        GObject.idle_add(self.on_next, None)

    def on_next(self, widget):
        self.__logger.debug("On Next")
        self.__stop()
        val = self.__play_control.dequeue()
        if val != None:
            self.__play(val)
        else:
            self.__active_wrapper = None
            self.__curr_track = None
            self.__curr_track_selected = None

        (tv1, tv2) = self.__get_path_pair()
        iter1 = self.treestore.get_iter(tv2)
        name = self.treestore.get_value(iter1, 0)

        l = []

        if name == self.HISTORY:
            self.__logger.debug("Viewing History")
            l = self.__play_control.get_history()
        elif name == self.QUEUE:
            self.__logger.debug("Viewing Queue")
            l = self.__play_control.get_queue()

        for x in l:
            self.__current_list.append(x)

    def on_prev(self, widget):
        self.__logger.debug("ON PREV")
        pos = self.__get_position()
        if pos < 2000:
            self.__logger.debug("Go to previous track.")
            val = self.__play_control.requeue()
            if val != None:
                #go to the previous song in the list
                self.__stop()
                self.__play(val)
        else:
            self.on_seek(None, None, 0)

    def on_seek(self, widget, scroll, value):
        self.__logger.debug("Seeking to position in song")
        if self.__active_wrapper != None:
            self.__active_wrapper.set_norm_position(value)

    def __get_position(self):
        if self.__active_wrapper != None:
            return self.__active_wrapper.get_position()
        return -1

    def on_pause(self, widget):
        self.__logger.debug("ON PAUSE")
        state = self.__pause()
        if state == spotipy.PlayState.PAUSED:
            widget.set_stock_id("gtk-media-play")
        elif state == spotipy.PlayState.PLAYING:
            widget.set_stock_id("gtk-media-pause")

    def play_playlist(self, tv, p, c):
        self.__logger.debug("PLAY PLAYLIST")
        (tv1, tv2) = self.__get_path_pair()
        iter1 = self.treestore.get_iter(tv2)
        name = self.treestore.get_value(iter1, 0)
        pl = self.treestore.get_value(iter1, 1)
        
        self.__play_control.clearlist()
        
        for x in self.__current_list:
            self.__play_control.queue_track_to_list(x)
            
        self.__play_control.shuffle()
        val = self.__play_control.dequeue()
        if val != None:
            self.__stop()
            self.__play(val)
            #self.__webkit_track_list.execute_script("playing('" + val.get_uri() + "')")

    def __pause(self):
        """ Pauses playback of a track """
        self.__logger.debug("__PAUSE")
        state = spotipy.PlayState.NULL
        if self.__active_wrapper != None:
            state = self.__active_wrapper.pause()
        return state

    def stop(self):
        GObject.idle_add(self.__stop)

    def get_shuffle(self, value):
        return self.__play_control.get_shuffle()
    def set_shuffle(self, value):
        if self.__play_control.get_shuffle() != value:
            GObject.idle_add(self.on_shuffle, self.builder.get_object('shuffle_button'))

    def repeat(self):
        GObject.idle_add(self.on_repeat, self.builder.get_object('repeat_button'))
    def next(self):
        GObject.idle_add(self.on_next, self.builder.get_object('next_button'))
    def prev(self):
        GObject.idle_add(self.on_prev, self.builder.get_object('prev_button'))
    def playpause(self):
        GObject.idle_add(self.on_pause, self.builder.get_object('play_button'))

    def __stop(self):
        """ Stops playback of a track """
        self.__logger.debug("__STOP")
        if self.__curr_track != None and self.__lastfm != None:
            duration = self.__curr_track.get_wrapper().get_duration()
            time_position = self.__curr_track.get_wrapper().get_position()
            self.__lastfm.track_playback_ended(self.__curr_track, duration, time_position)
            self.__mpris.track_playback_ended(self.__curr_track, duration, time_position)
            self.__webkit_track_list.execute_script("stopped()")
    
        self.__curr_track = None
        if self.__active_wrapper != None:
            self.__active_wrapper.stop()
        self.builder.get_object('scale1').set_sensitive(False)
        self.builder.get_object('play_button').set_stock_id("gtk-media-play")
        time_label = self.builder.get_object('curr_time')
        time_label.set_text("")
        time_label = self.builder.get_object('duration_time')
        time_label.set_text("")
        val = []
        if self.__status_bar_count > 0:
            self.__status_bar_count -= 1
            self.builder.get_object('statusbar1').pop(self.status_context)

    def on_shuffle_menu(self, widget):
        pass
    def on_repeat_menu(self, widget):
        pass

    def on_shuffle(self, widget):
        self.__logger.debug("ON SHUFFLE")
        """This function needs some work, but overall it works..."""        
        self.__play_control.set_shuffle(widget.get_active())

        #if we are in shuffle mode, add all items...
        found = False or widget.get_active()
        ll = []
        for x in self.__play_control.get_curr_playlist():
            if x == self.__play_control.get_curr():
                found = True
            if found:
                ll.append(x)

        self.__play_control.clearlist()
        self.__play_control.queuelist(ll)

    def on_repeat(self, widget):
        self.__logger.debug("ON REPEAT")
        self.__play_control.set_repeat(widget.get_active())

    def __play(self, val):
        """ Starts playback of a track """
        self.__logger.debug("__PLAY")
        scale = self.builder.get_object('scale1')
        scale.set_value(0)
        time_label = self.builder.get_object('curr_time')
        time_label.set_text("0:00")
        self.builder.get_object ("play_button").set_stock_id("gtk-media-pause")
        
        self.builder.get_object('scale1').set_sensitive(True)
        self.__curr_track_selected = val
        #print self.__curr_track_selected.get_uri()
        val = val.play()
        self.__status_bar_count += 1
        self.builder.get_object('statusbar1').push(self.status_context, val.get_artist().get_name() + " [ " + val.get_album().get_name() + " ] - " + val.get_title())
        to_print = "[ " + str(type(self.__active_wrapper)) + " ]: " + val.get_artist().get_name() + " [ " + val.get_album().get_name() + " ] - " + val.get_title()
        self.__logger.info(to_print)
        self.__active_wrapper = val.get_wrapper()
        self.__curr_track = val
        if self.__lastfm != None:
            self.__lastfm.track_playback_started(val)
        if self.__mpris != None:
            self.__mpris.track_playback_started(val)
        
        if self.__notify != None:
            self.__notify.update (val.get_artist().get_name(), val.get_title(), None)
            self.__notify.show()

        self.__webkit_track_list.execute_script("playing('" + self.__curr_track_selected.get_uri() + "')")
        #print self.__curr_track_selected.get_uri()
        try:
            if self.__lastfm != None:
                art = self.__lastfm.get_image(val, self.__update_image_callback)
        except:
            import traceback
            traceback.print_exc(file=sys.stderr)
            #self.__logger.warning("Getting Album Art: %s", str(ex))
        try:
            if self.__lastfm != None:
                bio = self.__lastfm.get_bio_content(val, self.__update_bio_callback)
            #bio = self.__lastfm.get_album_bio_content(val, self.__update_album_bio_callback)
        except Exception as ex:
            self.__logger.warning("Getting Artist/Album Bio: %s", str(ex))

    def __show_player(self):
        window = self.builder.get_object ("MainWindow")
        window.show()
        pass
    def __update_image_callback_helper(self, art):
        if art != None:
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file(art)
                h = pixbuf.get_height()
                w = pixbuf.get_width()
                target_width = 200
                ratio = float(w) / float(target_width)
                h = float(h) / ratio
                scaled_buf = pixbuf.scale_simple(target_width, int(h), GdkPixbuf.InterpType.BILINEAR)
                self.builder.get_object ("album_art").set_from_pixbuf(scaled_buf)
            except Exception as ex:
                self.__logger.warning(str(ex))
        else:
            pass
    def __update_image_callback(self, art):
        if not self.__closing:
            GObject.idle_add(self.__update_image_callback_helper, art)

    def __nav_request(self, view, frame, req, act, dec):
        print req.get_uri()
        dec.ignore()
        return True

    def __webkit_msg(self, view, frame, title):
        if title.find("star://") == 0:
            self.__logger.debug("ON STARRED")
            title = title.replace("star://", "")
            if not title in self.__lookup:
                print title
                print self.__lookup
            else:
                f = self.__lookup[title]
                f.set_starred(not f.get_starred())
        elif title.find("track://") == 0:
            self.__spotipy_nav_request(view, frame, title, None, None)
        elif title.find("artist://") == 0:
            self.__spotipy_nav_request(view, frame, title, None, None)
        elif title.find("album://") == 0:
            self.__spotipy_nav_request(view, frame, title, None, None)
        elif title.find("playlist://") == 0:
            self.__spotipy_nav_request(view, frame, title, None, None)
        elif title.find("search://") == 0:
            self.__spotipy_nav_request(view, frame, title, None, None)

    def __spotipy_nav_request(self, view, frame, req, act, dec):
        if dec != None:
            dec.ignore()

        try:
            uri = req.get_uri().replace("%20", " ")
        except AttributeError:
            uri = req

        command = ""
        if uri.find("track://") == 0:
            uri = uri.replace("track://", "")
            command = "track"
        elif uri.find("artist://") == 0:
            #uri = uri.replace("artist://", "")
            command = "artist"
        elif uri.find("album://") == 0:
            #uri = uri.replace("album://", "")
            command = "album"
        elif uri.find("playlist://") == 0:
            uri = uri.replace("playlist://", "")
            command = "playlist"
        elif uri.find("search://") == 0:
            uri = uri.replace("search://", "")
            command = "search"

        if uri.find("file//") == 0:
            uri = uri.replace("file//", "file://")

        if command == "track":
            self.__nav_request_track(uri)
        elif command == "album":
            GObject.idle_add(self.__nav_request_album, uri.strip())
        elif command == "artist":
            GObject.idle_add(self.__nav_request_artist, uri.strip())
        elif command == "playlist":
            self.__nav_request_playlist(uri)
        elif command == "search":
            GObject.idle_add(self.__on_find_helper, uri.strip())

        return True

    def __nav_request_album(self, uri):
        val = self.__lookup[unicode(uri)]
        html = self.__create_html(val, [self.__display_album])
        self.__nav_control.add_new(NavigationCommand(uri, self.__current_list, None, html))
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

    def __nav_request_artist(self, uri):
        val = self.__lookup[unicode(uri)]
        html = self.__create_html(val, [self.__display_artist])
        self.__nav_control.add_new(NavigationCommand(uri, self.__current_list, None, html))
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

    def __nav_request_playlist(self, uri):
        pass

    #def __nav_request_search(self, uri):
    #    GObject.idle_add(self.__on_find_helper, uri.strip())

    def __nav_request_track(self, uri):
        val = self.__lookup[unicode(uri)]
        index = 0
        for x in self.__current_list:
            if x.get_uri() == uri:
                break
            index += 1

        self.__play_control.clearlist()

        curr = self.__lookup[uri]
        found = False
        if self.__play_control.get_shuffle():
            found = True

        ll = []
        for x in self.__current_list:
            if x == curr:
                found = True
            if found:
                ll.append(x)

        if curr != None:
            if curr in ll:
                ll.remove(curr)
            self.__play_control.queue_to_front(curr)

        self.__play_control.queuelist(ll)
        val = self.__play_control.dequeue()
        self.__stop()
        self.__play(val)

    def __update_album_bio_callback_helper(self, bio):
        if bio != None:
            bio = bio.replace("\n", "<br />").replace("\r", "")
            v = self.builder.get_object("track_box").get_vadjustment()
            h = self.builder.get_object("track_box").get_hadjustment()
            if v != None:
                v.set_value(0)
            if h != None:
                h.set_value(0)
            self.__webkit_track_list.load_string(bio, "text/html", "utf-8", "")
        else:
            self.__webkit_track_list.load_string("", "text/html", "utf-8", "")

    def __update_album_bio_callback(self, bio):
        GObject.idle_add(self.__update_album_bio_callback_helper, bio)

    def __update_bio_callback_helper(self, bio):
        v = self.builder.get_object("bio_box").get_vadjustment().set_value(0)
        h = self.builder.get_object("bio_box").get_hadjustment().set_value(0)
        if bio != None:
            bio = bio.replace("\n", "<br />").replace("\r", "")
            self.__webkit_artist_bio.load_string(bio, "text/html", "utf-8", "")
        else:
            self.__webkit_artist_bio.load_string("", "text/html", "utf-8", "")

    def __update_bio_callback(self, bio):
        GObject.idle_add(self.__update_bio_callback_helper, bio)

    def __get_path_pair(self):
        """ Gets the selected item of the treeview and listview """
        try:
            w = self.builder.get_object ("treeview2")
            sel = w.get_selection()
            (tm, i) = sel.get_selected()
            tv_path2 = self.treestore.get_path(i)
            str_tv_path2 = str(tv_path2)
        except:
            tv_path2 = None

        return (None, tv_path2)

    def fill_tracks(self, w):
        """ Fills the listview based on the selected treeview item """
        self.__logger.debug("FILL TRACKS")
        del self.__current_list[:]
        #self.__lookup.clear()
        self.builder.get_object ("notebook1").set_current_page(0)
        try:
            (tm, i) = w.get_selected()
            name = self.treestore.get_value(i, 0)
            pl = self.treestore.get_value(i, 1)
        except:
            return

        label = self.builder.get_object ("user_name_label")
        if pl != None:
            label.set_text(pl.get_wrapper().get_username())
        else:
            label = getpass.getuser()

        l = None
        f = [self.__create_tracks_table]
        uri = "playlist://"
        result_list = []
        if name == self.HISTORY:
            l = self.__play_control.get_history()
            result_list.append(Result(l))
        elif name == self.QUEUE:
            l = self.__play_control.get_queue()
            result_list.append(Result(l))
        elif name == self.SEARCH_RESULTS:
            self.__build_search_html(self.__search_result_list)
            return
        elif name == self.STARRED:
            l = []
            for a in self.__wrappers:
                l.extend(a.get_starred().get_tracks())
            result_list.append(Result(l))
        elif pl != None:
            l = pl.get_tracks()
            result_list.append(Result(l))

        html = self.__create_html(result_list, f)
        self.__nav_control.add_new(NavigationCommand(uri, self.__current_list, None, html))
        can_go_back = self.__nav_control.can_get_prev()
        can_go_next = self.__nav_control.can_get_next()
        self.builder.get_object('nav_next_button').set_sensitive(can_go_next)
        self.builder.get_object('nav_back_button').set_sensitive(can_go_back)

    def __create_albums_table(self, l):
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th>Artist</th><th>Album</th><th>Year</th></tr></thead>\n")

        append_count = 0
        for r in l:
            for x in r.get_albums():
                year = str(x.get_year())
                if year == "0":
                    year = ""
                html.append(u"<tr id='" + x.get_uri() + "' ondblclick='send(\"album://" + x.get_uri() + "\");'><td><a href='artist://" + x.get_artist().get_uri() + "'>" + x.get_artist().get_name() + u"</a></td><td><a href='album://" + x.get_uri() + "'>" + x.get_name() + u"</a></td><td>" + year + "</td></tr>\n")
                self.__lookup[ "album://" + x.get_uri() ] = x
                self.__lookup[ "artist://" + x.get_artist().get_uri() ] = x.get_artist()
                append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __display_artist(self, artist):
        del self.__current_list[:]
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th>Artist</td><th>Album</th><th>Year</th></tr></thead>\n")

        albums = {}
        
        append_count = 0
        for x in artist.get_albums():
            type = x.get_type()
            if artist.get_name() != x.get_artist().get_name():
                type = Album.UNKNOWN
            key = str(type) + str(x.get_year()) + x.get_name()
            if not key in albums:
                albums[key] = []
            albums[key].append(x)

        for key in sorted(albums.keys(), None, None, True):
            #print key
            if len(albums[key]) > 1:
                pass
            else:
                pass
            for x in albums[key]:
                year = str(x.get_year())
                if year == "0":
                    year = ""
                html.append(u"<tr id='" + x.get_uri() + "' ondblclick='send(\"album://" + x.get_uri() + "\");'><td><a href='artist://" + x.get_artist().get_uri() + "'>" + x.get_artist().get_name() + u"</a></td><td><a href='album://" + x.get_uri() + "'>" + x.get_name() + u"</a></td><td>" + year + "</td></tr>\n")
                self.__lookup[x.get_uri()] = x
                self.__lookup[ "album://" + x.get_uri() ] = x
                self.__lookup[ "artist://" + x.get_artist().get_uri() ] = x.get_artist()
                self.__current_list.append(x)
                append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __display_album(self, album):
        del self.__current_list[:]
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th></th><th>Title</th><th>Artist</td><th>Album</th></tr></thead>\n")

        append_count = 0
        for x in album.get_tracks():
            checked = ""
            if x.get_starred():
                checked = "checked='true'"

            html.append(u"<tr id='" + x.get_uri() + "' ondblclick='send(\"track://" + x.get_uri() + "\");'><td><input onclick='send(\"star://" + x.get_uri() + "\");' type='checkbox' name='' " + checked + " value='" + x.get_uri() + "' /></td><td><a href='track://" + x.get_uri() + "'>" + x.get_name() + u"</a></td><td><a href='artist://" + x.get_artist().get_uri() + "'>" + x.get_artist().get_name() + u"</a></td><td><a href='album://" + x.get_album().get_uri() + "'>" + x.get_album().get_name() + u"</a></td></tr>\n")
            self.__lookup[x.get_uri()] = x
            self.__lookup[ "album://" + x.get_album().get_uri() ] = x.get_album()
            self.__lookup[ "artist://" + x.get_artist().get_uri() ] = x.get_artist()
            self.__current_list.append(x)
            append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __create_suggestions_table(self, l):
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th>Search Suggestions</th></tr></thead>\n")

        append_count = 0
        for r in l:
            if r.get_suggestion() != None and r.get_suggestion() != "":
                html.append(u"<tr ondblclick='send(\"search://" + r.get_suggestion() + "\");'><td><a href='search://" + r.get_suggestion() + "'>" + r.get_suggestion() + u"</a></td></tr>\n")
                append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __create_artists_table(self, l):
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th>Artist</th></tr></thead>\n")

        append_count = 0
        for r in l:
            for x in r.get_artists():
                html.append(u"<tr id='" + x.get_uri() + "' ondblclick='send(\"artist://" + x.get_uri() + "\");'><td><a href='artist://" + x.get_uri() + "'>" + x.get_name() + u"</a></td></tr>\n")
                self.__lookup[ "artist://" + x.get_uri() ] = x
                append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __create_tracks_table(self, l):
        del self.__current_list[:]
        html = []
        html.append(u"<table class='spotipy tableWithFloatingHeader'>\n")
        html.append(u"<thead><tr><th></th><th>Title</th><th>Artist</td><th>Album</th></tr></thead>\n")

        append_count = 0
        for r in l:
            for x in r.get_tracks():
                checked = ""
                if x.get_starred():
                    checked = "checked='true'"
    
                html.append(u"<tr id='" + x.get_uri() + "' ondblclick='send(\"track://" + x.get_uri() + "\");'><td><input onclick='send(\"star://" + x.get_uri() + "\");' type='checkbox' name='' " + checked + " value='" + x.get_uri() + "' /></td><td><a href='track://" + x.get_uri() + "'>" + x.get_name() + u"</a></td><td><a href='artist://" + x.get_artist().get_uri() + "'>" + x.get_artist().get_name() + u"</a></td><td><a href='album://" + x.get_album().get_uri() + "'>" + x.get_album().get_name() + u"</a></td></tr>\n")
                self.__lookup[x.get_uri()] = x
                self.__lookup[ "album://" + x.get_album().get_uri() ] = x.get_album()
                self.__lookup[ "artist://" + x.get_artist().get_uri() ] = x.get_artist()
                self.__current_list.append(x)
                append_count += 1

        html.append(u"</table>\n")
        if append_count <= 0:
            del html[:]
        return html

    def __create_html(self, l, functions):
        if l == None:
            return ""

        try:
            path = os.path.realpath(__file__)
            script_dir = os.path.dirname(path)
            html = []
            html.append(u"<html><head>\n")
            #load CSS
            html.append(u"<style type='text/css'>\n")

            css_dir = os.path.join(script_dir, "gui/css/")
            for css in sorted(os.listdir(unicode(css_dir))):
                fil = os.path.join(css_dir, css)
                if os.path.isfile(fil):
                    f = codecs.open(fil, "r", "utf-8")
                    html.append(f.read())

            html.append(u"</style>\n")
            html.append("""<script type="text/javascript">
                function send(msg) {
                    document.title = "null";
                    document.title = msg;
                }</script>""");
            html.append(u"</head><body>\n")

            for f in functions:
                html.extend(f(l))

            script_dir = os.path.join(script_dir, "gui/js/")
            for js in sorted(os.listdir(unicode(script_dir))):
                fil = os.path.join(script_dir, js)
                if os.path.isfile(fil):
                    f = codecs.open(fil, "r", "utf-8")
                    html.append(u"<script>" + f.read() + u"</script>")

            html.append("""<script type="text/javascript">
                    $(document).ready(function () {
                    $("table").stickyTableHeaders();
                });
                </script>""")

            html.append(u"</body></html>")

            html = ''.join(html)
            self.builder.get_object("track_box").get_vadjustment().set_value(0)
            self.builder.get_object("track_box").get_hadjustment().set_value(0)
            self.__curr_html = html
            self.__webkit_track_list.load_string(html, "text/html", "utf-8", "")
        except Exception as ex:
            #raise
            self.__logger.warning("%s", str(ex))
        return self.__curr_html

    def on_spotify_update(self, w):
        password = self.builder.get_object ("spotify_password")
        username = self.builder.get_object ("spotify_username")
        quality = self.builder.get_object("spotify_audio_quality")
        self.__logger.info("Setting Spotify Audio Quality: " + str(quality.get_active()))
        active = quality.get_active()
        
        if active == 0:
            active = 96
        elif active == 1:
            active = 160
        else:
            active = 320

        spotipy.settings.SPOTIFY_PASSWORD = password.get_text()
        spotipy.settings.SPOTIFY_USERNAME = username.get_text()
        spotipy.settings.SPOTIFY_BITRATE = active

        try:
            SpotifyWrapper.get_instance().set_preferred_bitrate(active)
        except Exception as ex:
            self.__logger.warning(str(ex))
        spotipy.settings.save_values()

    def on_lastfm_update(self, w):
        password = self.builder.get_object ("lastfm_password")
        username = self.builder.get_object ("lastfm_username")

        #update credentials
        spotipy.settings.LASTFM_PASSWORD = password.get_text()
        spotipy.settings.LASTFM_USERNAME = username.get_text()
                
        #login
        if self.__lastfm != None:
            self.__lastfm.login(spotipy.settings.LASTFM_USERNAME, spotipy.settings.LASTFM_PASSWORD)
        spotipy.settings.save_values()

    def on_spotify_cred(self, w):
        pref = self.builder.get_object ("spotify_login")
        pref.set_default_response(Gtk.ResponseType.CANCEL)
        password = self.builder.get_object ("spotify_password")
        username = self.builder.get_object ("spotify_username")
        quality = self.builder.get_object("spotify_audio_quality")
        active = 0
        if spotipy.settings.SPOTIFY_BITRATE == 160:
            active = 1
        elif spotipy.settings.SPOTIFY_BITRATE == 320:
            active = 2
        else:
            active = 0
        quality.set_active(active)

        password.set_text(spotipy.settings.SPOTIFY_PASSWORD)
        username.set_text(spotipy.settings.SPOTIFY_USERNAME)

        pref.run()
        pref.hide()

    def on_lastfm_cred(self, w):
        pref = self.builder.get_object ("lastfm_login")
        pref.set_default_response(Gtk.ResponseType.CANCEL)
        password = self.builder.get_object ("lastfm_password")
        username = self.builder.get_object ("lastfm_username")

        password.set_text(spotipy.settings.LASTFM_PASSWORD)
        username.set_text(spotipy.settings.LASTFM_USERNAME)

        pref.run()
        pref.hide()

    def quit(self, w):
        """ Quit the application """
        Notify.uninit ()

        self.__logger.debug("QUIT")
        self.__closing = True
        
        self.__stop()
        if self.__lastfm != None:
            self.__lastfm.stop()
            self.__lastfm = None
        if self.__mpris != None:
            self.__mpris.stop()
            self.__mpris = None

        for x in self.__wrappers:
            try:
                x.shutdown()
            except Exception as ex:
                self.__logger.warning("Error during shutdown: %s", str(ex))
        for t in threading.enumerate():
            if t.name.find("SpotiPy") == 0:
                t.stop_thread()
                t.join()
        Gtk.main_quit()

    def __time_to_str(self, length):
        if length <= 0:
            return "0:00"
        length = length / 1000
        lg0 = length / 60
        lg1 = length % 60

        if lg0 >= 0 and lg0 < 10:
            lg0 = str(lg0)

        if lg1 >= 0 and lg1 < 10:
            lg1 = '0' + str(lg1)

        lg = str(lg0) + ':' + str(lg1)
        return lg

    #def __import_plugins(self, directory):
    #    """Load Plugins"""
    #    if os.path.exists(directory):
    #        for filename in os.listdir (directory):
    #            # Ignore subfolders
    #            if os.path.isdir (os.path.join(directory, filename)):
    #                continue
    #            else:
    #                if re.match(r".*\.py$", filename):
    #                    #print ('Initialising plugin : ' + filename)
    #                    __import__(re.sub(r".py", r"", filename))

    def on_about(self, w):
        """ Displays dialog box """
        import spotipy.core
        self.about = self.builder.get_object ("about_spotipy")
        self.about.set_default_response(Gtk.ResponseType.CANCEL)

        self.about.set_version(spotipy.__version__)
        self.about.set_license(spotipy.__license__)
        self.about.run()
        self.about.hide()
