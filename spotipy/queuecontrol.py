# -*- coding: utf-8 -*-

import random

class QueueControl(object):

    __index = 0
    __history = []
    __play_list = []
    __queue_list = []
    __repeat = False
    __shuffle = False
    __curr_track = None
    __curr_playlist = []

    def __init__(self, tracks=[]):
        self.__play_list.extend(tracks)

    def clearall(self):
        del self.__play_list[:]
        del self.__queue_list[:]

    def clearlist(self):
        del self.__play_list[:]
        del self.__curr_playlist[:]

    def queue_to_front(self, track):
        self.__queue_list.insert(0, track)

    def queuetrack(self, track):
        self.__queue_list.append(track)

    def queue_track_to_list(self, track):
        self.__play_list.append(track)
        self.__curr_playlist.append(track)

    def queuelist(self, tracks):
        self.clearlist()
        self.__play_list.extend(tracks)
        self.__curr_playlist.extend(tracks)
        if self.__shuffle:
            random.shuffle(self.__play_list)
    
    def shuffle(self):
        if self.__shuffle:
            random.shuffle(self.__play_list)

    #def sort(self):
    #    if self.__shuffle:
    #        random.shuffle(self.__play_list)
    #    else:
    #        self.__play_list.sort()

    def get_history(self):
        return self.__history

    def get_queue(self):
        l = []
        l.extend(self.__queue_list)
        l.extend(self.__play_list)
        return l

    def dequeue(self):
        if self.__repeat:
            return self.__curr_track

        retVal = None
        if self.__index + 1 >= len(self.__history):
            if len(self.__queue_list) > 0:
                retVal = self.__queue_list.pop(0)
            elif len(self.__play_list) > 0:
                retVal = self.__play_list.pop(0)
            if retVal != None:
                self.__history.append(retVal)
                self.__index = len(self.__history)
        else:
            self.__index += 1
            retVal = self.__history[self.__index]
        self.__curr_track = retVal
        return retVal

    def requeue(self):
        if self.__repeat:
            return self.__curr_track

        if self.__index > 0:
            self.__index -= 1
            return self.__history[self.__index]
        return None

    def set_shuffle(self, value):
        self.__shuffle = value

    def get_shuffle(self):
        return self.__shuffle

    def get_curr(self):
        return self.__curr_track

    def get_curr_playlist(self):
        return self.__curr_playlist

    def set_repeat(self, value):
        self.__repeat = value

    def get_repeat(self):
        return self.__repeat

