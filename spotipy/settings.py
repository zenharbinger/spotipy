"""
Available settings and their default values.

.. warning::

    Do *not* change settings directly in :mod:`spotipy.settings`. Instead, add a
    file called ``~/.config/spotipy/settings.py`` and redefine settings there.
"""

#: Your `Last.fm <http://www.last.fm/>`_ username.
#:
#: Used by :mod:`spotipy.frontends.lastfm`.
LASTFM_USERNAME = u''

#: Your `Last.fm <http://www.last.fm/>`_ password.
#:
#: Used by :mod:`spotipy.frontends.lastfm`.
LASTFM_PASSWORD = u''

#: Path to folder with local music.
#:
#: Used by :mod:`spotipy.backends.local`.
#:
#: Default::
#:
#:    # Defaults to asking glib where music is stored, fallback is ~/music
#:    LOCAL_MUSIC_PATH = None
LOCAL_MUSIC_PATH = None

#: Your Spotify Premium username.
#:
#: Used by :mod:`spotipy.backends.spotify`.
SPOTIFY_USERNAME = u''

#: Your Spotify Premium password.
#:
#: Used by :mod:`spotipy.backends.spotify`.
SPOTIFY_PASSWORD = u''

#: Spotify preferred bitrate.
#:
#: Available values are 96, 160, and 320.
#:
#: Used by :mod:`spotipy.backends.spotify`.
#
#: Default::
#:
#:     SPOTIFY_BITRATE = 160
SPOTIFY_BITRATE = 160

DESKTOP_FILE="/usr/share/applications/spotipy.desktop"
