# Absolute import needed to import ~/.config/spotipy/settings.py and not ourselves
from __future__ import absolute_import
from copy import copy
import getpass
import glib
import os
import sys
import json
from spotipy import SETTINGS_PATH, SETTINGS_FILE, SETTINGS_JSON_FILE

class SettingsProxy(object):
    def __init__(self, default_settings_module):
        self.default = self._get_settings_dict_from_module(
            default_settings_module)
        self.local = self._get_local_settings()
        self.runtime = {}
        self.__read_values()

    def _get_local_settings(self):
        if not os.path.isfile(SETTINGS_FILE):
            return {}
        sys.path.insert(0, SETTINGS_PATH)
        # pylint: disable = F0401
        import settings as local_settings_module
        # pylint: enable = F0401
        return self._get_settings_dict_from_module(local_settings_module)

    def _get_settings_dict_from_module(self, module):
        settings = filter(lambda (key, value): self._is_setting(key),
            module.__dict__.iteritems())
        return dict(settings)

    def _is_setting(self, name):
        return name.isupper()

    @property
    def current(self):
        current = copy(self.default)
        current.update(self.local)
        current.update(self.runtime)
        return current

    def set_values(self, val):
        self.runtime.update(val)

    def get_values(self):
        return self.current

    def __read_values(self):
        try:
            f = open(SETTINGS_JSON_FILE, 'r')
            json_obj = json.load(f)
            f.close()
            #print json_obj
            self.runtime.update(json_obj)
        except Exception as ex:
            #print str(ex)
            pass

    def save_values(self):
        x = self.get_values()
        d = {}
        for a in x.keys():
            if a.find("_") != 0:
                d[a] = x[a]

        json_str = json.dumps(d, sort_keys=True, indent=4)
        f = open(SETTINGS_JSON_FILE, 'w')
        f.write(json_str)
        f.flush()
        f.close()

    def __getattr__(self, attr):
        if not self._is_setting(attr):
            return
        if attr not in self.current:
            raise Exception(u'Setting "%s" is not set.' % attr)
        value = self.current[attr]
        #if isinstance(value, basestring) and len(value) == 0:
        #    raise Exception(u'Setting "%s" is empty.' % attr)
        if not value:
            return value
        if attr.endswith('_PATH') or attr.endswith('_FILE'):
            value = os.path.expanduser(value)
            value = os.path.abspath(value)
        return value

    def __setattr__(self, attr, value):
        if self._is_setting(attr):
            self.runtime[attr] = value
        else:
            super(SettingsProxy, self).__setattr__(attr, value)

